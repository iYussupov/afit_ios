//
//  AppDelegate.swift
//  AFit
//
//  Created by Ingwar on 3/5/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import DrawerController
import GoogleMaps
import UserNotifications
import SwiftyJSON
import Alamofire
import FBSDKCoreKit
import VK_ios_sdk
import ImageSlideshow

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var drawerController: DrawerController!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        application.applicationIconBadgeNumber = 0
        
        let defaults = UserDefaults.standard
        
        if defaults.string(forKey: "default_app_name") != nil, defaults.string(forKey: "default_app_slug") != nil {
            Constants.APP_NAME = defaults.string(forKey: "default_app_name")!
            Constants.URL_SLUG = defaults.string(forKey: "default_app_slug")!
        }
        
        Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_FAMILY)/").responseJSON { response in
            
            if let data = response.result.value {
                
                let objects = JSON(data)
                
                for object in objects {
                    
                    let family = object.1[Constants.APP_FAMILY]
                    
                    if family != JSON.null {
                        
                        defaults.setValue(family.rawString()!, forKey: "app_family_array")
                        
                        defaults.setValue(Constants.APP_NAME, forKey: "default_app_name")
                        
                        defaults.setValue(Constants.URL_SLUG, forKey: "default_app_slug")
                        
                        defaults.synchronize()
                            
                        Constants.APP_FAMILY_ARRAY = family
                        
                    }
                        
                }
                
            }
            
        }
        
        // Google maps api key
        GMSServices.provideAPIKey(Constants.Google_API)
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        var centerViewController:UIViewController = UIViewController()
        
        centerViewController = mainStoryboard.instantiateViewController(withIdentifier: "LaunchVC") as! LaunchVC
        
        let leftSideNavController = mainStoryboard.instantiateViewController(withIdentifier: "LeftSideVC") as! LeftSideVC
        
        let leftSideNav = UINavigationController(rootViewController: leftSideNavController)
        let centerNav = UINavigationController(rootViewController: centerViewController)
        
        self.drawerController = DrawerController(centerViewController: centerNav, leftDrawerViewController: leftSideNav)
        
        self.drawerController.showsShadows = true
        self.drawerController.maximumLeftDrawerWidth = 320.0
        self.drawerController.restorationIdentifier = "Drawer"
        self.drawerController.openDrawerGestureModeMask = .All
        self.drawerController.closeDrawerGestureModeMask = .All
        
        self.window?.rootViewController = self.drawerController
        self.window?.makeKeyAndVisible()
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 8) {
            // iOS 10 support
            if #available(iOS 10, *) {
                UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
                application.registerForRemoteNotifications()
            }
            // iOS 9 support
            else if #available(iOS 9, *) {
                UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
        
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        VKSdk.initialize(withAppId: Constants.VC_id)
        
//        Kontakt.setAPIKey("\(Constants.Kontakt_API)")
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        VKSdk.processOpen(url, fromApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!)
        
        
        return true
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
        application.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        
        Constants.DEVICE_TOKEN = deviceTokenString
        
        // create the request
        let url = URL(string: "\(Constants.URL_BASE)\(Constants.URL_SLUG)push/savetoken/?device_token=\(deviceTokenString)&device_type=ios&channels_id=1")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let urlConnection = NSURLConnection(request: request, delegate: self)
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask(rawValue: UInt(checkOrientation(viewController: self.window?.rootViewController)))
    }
    
    func checkOrientation(viewController:UIViewController?)-> Int{
        
        if(viewController == nil){
            
            return Int(UIInterfaceOrientationMask.portrait.rawValue)
            
        } else if (viewController is ViewerVC || viewController is FullScreenSlideshowViewController){
            
            return Int(UIInterfaceOrientationMask.all.rawValue)
            
        } else {
            
            return checkOrientation(viewController: viewController!.presentedViewController)
        }
    }
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        if let aps = data["aps"] {
            let objects = JSON(aps)
            if let postID = objects["post_id"].string, postID != "" {
                
                Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_POSTS)/\(postID)").responseJSON { response in
                    
                    
                    if let data = response.result.value {
                        
                        let object = JSON(data)
                        
                        let id = object["id"].int
                        
                        let timeStamp = object["timestamp"].int
                        
                        let title = object["title"]["rendered"].string
                        
                        var content = ""
                        
                        do {
                            content = try object["content"]["rendered"].string!.convertHtmlSymbols()!
                        } catch {
                            
                        }
                        
                        let featuredImg = object["featured_image_thumbnail_url"].string
                        
                        let date = object["date"].string
                        
                        let dateFormatter = DateFormatter()
                        
                        let createdDate = dateFormatter.dateFromSwapiString(dateString:date!)
                        
                        let category = object["post_category_name"].string
                        
                        let news = News(timeStamp:timeStamp, postId:id,title:title, content:content, featuredImg:featuredImg, category:category, date: createdDate)
                        
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let centerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewsDetailVC") as! NewsDetailVC
                        centerViewController.news = news
                        centerViewController.fromPushDirectly = true
                        centerViewController.toggleRightDrawer = true
                        
                        let centerNav = UINavigationController(rootViewController: centerViewController)
                        
                        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.drawerController!.centerViewController = centerNav
                        
                    }
                    
                }
            
            }
        }
        
        
    }

}

