//
//  NewMainVC.swift
//  AFit
//
//  Created by Dev1 on 11/28/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AVFoundation
//import KontaktSDK
var startedFlag: Bool = false

class NewMainVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var clubSelectLbl: UIStackView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationTitleLbl: UILabel!
    @IBOutlet weak var clubTitle: UILabel!
    
    var musicPlayer: AVAudioPlayer!
    var menus = [Menus]()
    var preventAnimation = Set<IndexPath>()
    let cellSpacingHeight: CGFloat = 8
    var isHeightCalculated: Bool = false
    var viewInitialized:Bool = false
//    var beaconManager: KTKBeaconManager!
    
    static var instance:NewMainVC?
    
    override func awakeFromNib() {
        type(of: self).instance = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .BezelPanningCenterView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
//        // Initiate Beacon Manager
//        beaconManager = KTKBeaconManager(delegate: self)
//        beaconManager.requestLocationAlwaysAuthorization()
//        
//        // Region
//        let proximityUUID = NSUUID(uuidString: "f7826da6-4fa2-4e98-8024-bc5b71e0893e")
//        let region = KTKBeaconRegion(proximityUUID: proximityUUID! as UUID, identifier: "region-identifier")
//        
//        // Region Properties
//        region.notifyEntryStateOnDisplay = true
//        
//        //beaconManager.stopMonitoringForAllRegions()
//        
//        // Start Ranging
//        beaconManager.startRangingBeacons(in: region)
//        beaconManager.startMonitoring(for: region)
//        beaconManager.requestState(for: region)
        
        
        
        collection.dataSource = self
        collection.delegate = self
        
        self.navigationTitleLbl.alpha = 0.0
        self.navigationTitleLbl.text = Constants.appName.uppercased()
        
        self.parseMenuData()
        
        if Constants.APP_FAMILY_ARRAY.count < 2 {
            
            self.clubSelectLbl.isHidden = true
            self.clubSelectLbl.removeConstraints(self.clubSelectLbl.constraints)
            self.clubSelectLbl.translatesAutoresizingMaskIntoConstraints = false
            
            let flow:UICollectionViewFlowLayout = self.collection.collectionViewLayout as! UICollectionViewFlowLayout
            flow.sectionInset = UIEdgeInsetsMake(12.0, 16.0, 16.0, 16.0)
            
        } else {
            
            let defaults = UserDefaults.standard
            
            if defaults.object(forKey:"first_start") == nil {
            
                self.callClubSelectModal()
                
                defaults.set(true, forKey: "first_start")
                defaults.synchronize()
                
            }
        
        }
        
        if Constants.APP_NAME != "" {
            
            self.clubTitle.text = Constants.APP_NAME.uppercased()
        
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(NewMainVC.callClubSelectModal))
        self.clubTitle.isUserInteractionEnabled = true
        self.clubTitle.addGestureRecognizer(tap)
        
    }
    
//    func updatePanelsUI(){
//        self.lastPosition = -1
//        self.collection.isHidden = false
//        self.collection.reloadData()
//        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            self.viewInitialized = true
//        }
//    }
    
    func parseMenuData() {
        
        let objects = [[
                "title":"SCHEDULE".localized,
                "icon":""
            ],[
                "title":"NEWS / OFFERS".localized,
                "icon":""
            ],[
                "title":"SERVICES".localized,
                "icon":""
            ],[
                "title":"NOTIFICATIONS".localized,
                "icon":""
            ],[
                "title":"ABOUT US".localized,
                "icon":""
            ],[
                "title":"GALLERY".localized,
                "icon":""
            ],[
                "title":"OUR TEAM".localized,
                "icon":""
            ],[
                "title":"CONTACTS".localized,
                "icon":""
            ]]
        
        for object in objects {
            
            let menu = Menus(dictionary: object as AnyObject)
            
            self.menus.append(menu)
            
        }
        
        
        self.collection.reloadData()
    }
    

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenusCell", for: indexPath) as? MenusCell {
            
            let menu = menus[(indexPath as NSIndexPath).row]
            
            cell.configureCell(menu)
            
            let position = (indexPath as NSIndexPath).row
            
            
            if !self.viewInitialized {
            
                var k:CGFloat = 1.0
                if position % 2 == 0 {
                    k = -1.0
                }
            
                cell.alpha = 0.0
                let startCellX = cell.frame.origin.x + k * 15
                let startCellY = cell.frame.origin.y + 95
                cell.frame.origin.x = startCellX
                cell.frame.origin.y = startCellY

        
                UIView.animate(withDuration: 0.5, delay: Double(position + 1) * 0.075, options: [.curveEaseOut], animations: {
                
                    cell.alpha = 1.0
                
                }, completion: nil)
            
                UIView.animate(withDuration: 0.6, delay: Double(position + 1) * 0.075, options: [.curveEaseOut], animations: {
                
                    cell.frame.origin.x = startCellX - k * 15
                
                }, completion: nil)
            
                UIView.animate(withDuration: 0.6, delay: Double(position + 1) * 0.075, usingSpringWithDamping: 0.7, initialSpringVelocity: 2, options: .curveEaseInOut, animations: {
                    
                    cell.frame.origin.y = startCellY - 100
                    
                }, completion: nil)
                
            }
            
            
            return cell
            
        } else {
            
            return MenusCell()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let index = (indexPath as NSIndexPath).row
        let scaleFactor:CGFloat = 2
        let sizeVariation:CGFloat = scaleFactor*36
        
        let cell : UICollectionViewCell = collectionView.cellForItem(at: indexPath)!
        let startCellX = cell.frame.origin.x
        let startCellY = cell.frame.origin.y
        
        cell.layer.zPosition = 2
       
        if index % 2 == 0 {
            UIView.animate(withDuration: 0.1, delay: 0.0, options: [.curveEaseOut], animations: {
                
                cell.frame.origin.x = startCellX + sizeVariation + 10.0
                cell.frame.origin.y = startCellY + sizeVariation
                cell.transform = CGAffineTransform(scaleX: scaleFactor, y: scaleFactor)
                
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.1, delay: 0.0, options: [.curveEaseOut], animations: {
                
                cell.frame.origin.x = startCellX - sizeVariation - 10.0
                cell.frame.origin.y = startCellY + sizeVariation
                cell.transform = CGAffineTransform(scaleX: scaleFactor, y: scaleFactor)
                
            }, completion: nil)
        }
        
        UIView.animate(withDuration: 0.1, delay: 0.0, options: [.curveEaseOut], animations: {
                collectionView.alpha = 0.0
        }, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
            self.loadViewController(index: index)
        }
    }
    
    func loadViewController(index: Int ) {
        switch(index)  {
            
        case 0:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
            self.navigationController?.pushViewController(centerViewController, animated: true)
            
            break;
            
        case 1:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewsVC") as! NewsVC
            self.navigationController?.pushViewController(centerViewController, animated: true)
            
            break;
            
        case 2:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "ServicesVC") as! ServicesVC
            self.navigationController?.pushViewController(centerViewController, animated: true)
            
            break;
            
        case 3:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "PushVC") as! PushVC
            self.navigationController?.pushViewController(centerViewController, animated: true)
            
            break;
            
        case 4:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
            self.navigationController?.pushViewController(centerViewController, animated: true)
            
            break;
            
        case 5:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "PhotoGalleryVC") as! PhotoGalleryVC
            self.navigationController?.pushViewController(centerViewController, animated: true)
            
            break;
            
        case 6:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "TrainersVC") as! TrainersVC
            self.navigationController?.pushViewController(centerViewController, animated: true)
            
            break;
            
        case 7:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
            self.navigationController?.pushViewController(centerViewController, animated: true)
            break;
            
        default:
            
            print("\(menus[index]) is selected");
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return menus.count
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let paddingSpace:CGFloat = 40.0
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / 2
        var heightPerItem = widthPerItem * 0.95
        if heightPerItem < 160 {
            heightPerItem = 160
        }
        
        return CGSize(width: widthPerItem, height: heightPerItem)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        var labelsTransform = CATransform3DIdentity
        
        // PULL DOWN -----------------
        
        if offset <= 0 {
            self.navigationView.backgroundColor = UIColor(red: (118/255.0), green: (190/255.0), blue: (52/255.0), alpha: 0.0)
            
            self.navigationTitleLbl.alpha = 0.0
            
            labelsTransform = CATransform3DTranslate(labelsTransform, 0, -offset, 0)
            
            self.clubSelectLbl.layer.transform = labelsTransform
            
        } else {
            
            var alpha = 0.02 * offset
            
            if alpha < 0 {alpha = 0}
            if alpha > 1 {alpha = 1}
            
            self.navigationTitleLbl.alpha = alpha
            
            self.navigationView.backgroundColor = UIColor(red: (118/255.0), green: (190/255.0), blue: (52/255.0), alpha: alpha)

            
            labelsTransform = CATransform3DTranslate(labelsTransform, 0, max(-offset_HeaderStop, -offset), 0)
            
        }
        
        self.clubSelectLbl.layer.transform = labelsTransform
        
    }
    
    func callClubSelectModal() {
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ClubSelectVC") as! ClubSelectVC
        self.addChildViewController(popOverVC)
        
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    }
    
    func clubDidSelected() {
        
        if Constants.APP_NAME != "" {
            self.clubTitle.text = Constants.APP_NAME.uppercased()
        } else {
            self.clubTitle.text = ""
        }
        
    }
    
    
    @IBAction func openMenu(_ sender: AnyObject) {
        self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
    }
    
    @IBAction func openProfile(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
}



//extension NewMainVC: KTKBeaconManagerDelegate {
//    
//    func beaconManager(_ manager: KTKBeaconManager, didDetermineState state: CLRegionState, for region: KTKBeaconRegion) {
//        print("Did determine state \"\(state.rawValue)\" for region: \(region)")
//    }
//    
//    func beaconManager(_ manager: KTKBeaconManager, didChangeLocationAuthorizationStatus status: CLAuthorizationStatus) {
//        print("Did change location authorization status to: \(status.rawValue)")
//        
//    }
//    
//    func beaconManager(_ manager: KTKBeaconManager, monitoringDidFailFor region: KTKBeaconRegion?, withError error: Error?) {
//        print("Monitoring did fail for region: \(region)")
//        print("Error: \(error)")
//        beaconManager.startMonitoring(for: region!)
//    }
//    
//    func beaconManager(_ didStartMonitoringFormanager: KTKBeaconManager, didStartMonitoringFor region: KTKBeaconRegion) {
//        print("Did start monitoring for region: \(region)")
//    }
//    
//    func beaconManager(_ manager: KTKBeaconManager, didEnter region: KTKBeaconRegion) {
//        print("Did enter region: \(region)")
//    }
//    
//    func beaconManager(_ manager: KTKBeaconManager, didExitRegion region: KTKBeaconRegion) {
//        print("Did exit region \(region)")
//    }
//    
//    func beaconManager(_ manager: KTKBeaconManager, didRangeBeacons beacons: [CLBeacon], in region: KTKBeaconRegion) {
//        print("Did ranged \"\(beacons.count)\" beacons inside region: \(region)")
//        
////        if let closestBeacon = beacons.sorted(by: { $0.0.accuracy < $0.1.accuracy }).first, closestBeacon.accuracy > 0 {
//        for beacon in beacons{
//            print("Closest Beacon is M: \(beacon.major), m: \(beacon.minor) ~ \(beacon.accuracy) meters away.")
//        }
//    }
//}
