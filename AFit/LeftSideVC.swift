//
//  LeftSideVC.swift
//  AFit
//
//  Created by Dev1 on 3/9/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit

class LeftSideVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var menuItems:[String] = ["HOME".localized,"SCHEDULE".localized,"NEWS / OFFERS".localized,"NOTIFICATIONS".localized,"ABOUT US".localized,"GALLERY".localized,"SERVICES".localized,"OUR TEAM".localized,"CONTACTS".localized]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        //
        self.navigationController?.view.setNeedsLayout()
        //
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menuItems.count;
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        
        cell.menuItemLbl.text = menuItems[(indexPath as NSIndexPath).row].uppercased()
        
        if (indexPath as NSIndexPath).row == self.menuItems.count - 1 {
            cell.cellSeparatorView.isHidden = true
        } else {
            cell.cellSeparatorView.isHidden = false
        }
        
        return cell;
        
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 0.4)
        cell?.backgroundColor = UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 0.4)
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 0)
        cell?.backgroundColor = UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 0)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
//        for var index = 0; index <= menuItems.count; ++index {
//            let tempindexpath =  NSIndexPath(forRow: index, inSection: 0)
//            let cell = tableView.cellForRowAtIndexPath(tempindexpath)
//            cell?.backgroundColor = UIColor(red: 118/255, green: 190/255, blue: 52/255, alpha: 0.0)
//        }
//        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = UIColor(red: 118/255, green: 190/255, blue: 52/255, alpha: 0.0)

        
        switch((indexPath as NSIndexPath).row)  {
            
        case 0:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewMainVC") as! NewMainVC
            let centerNav = UINavigationController(rootViewController: centerViewController)
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.drawerController!.centerViewController = centerNav
            appDelegate.drawerController!.toggleDrawerSide(.left, animated: true, completion: nil)
            
            break;
            
        case 1:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
            let centerNav = UINavigationController(rootViewController: centerViewController)
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.drawerController!.centerViewController = centerNav
            appDelegate.drawerController!.toggleDrawerSide(.left, animated: true, completion: nil)
            
            break;
            
        case 2:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewsVC") as! NewsVC
            let centerNav = UINavigationController(rootViewController: centerViewController)
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.drawerController!.centerViewController = centerNav
            appDelegate.drawerController!.toggleDrawerSide(.left, animated: true, completion: nil)
            
            break;
            
        case 3:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "PushVC") as! PushVC
            let centerNav = UINavigationController(rootViewController: centerViewController)
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.drawerController!.centerViewController = centerNav
            appDelegate.drawerController!.toggleDrawerSide(.left, animated: true, completion: nil)
            
            break;

        case 4:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
            let centerNav = UINavigationController(rootViewController: centerViewController)
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.drawerController!.centerViewController = centerNav
            appDelegate.drawerController!.toggleDrawerSide(.left, animated: true, completion: nil)
            
            break;
            
        case 5:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "PhotoGalleryVC") as! PhotoGalleryVC
            let centerNav = UINavigationController(rootViewController: centerViewController)
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.drawerController!.centerViewController = centerNav
            appDelegate.drawerController!.toggleDrawerSide(.left, animated: true, completion: nil)
            
            break;
            
        case 6:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "ServicesVC") as! ServicesVC
            let centerNav = UINavigationController(rootViewController: centerViewController)
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.drawerController!.centerViewController = centerNav
            appDelegate.drawerController!.toggleDrawerSide(.left, animated: true, completion: nil)
            
            break;
            
        case 7:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "TrainersVC") as! TrainersVC
            let centerNav = UINavigationController(rootViewController: centerViewController)
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.drawerController!.centerViewController = centerNav
            appDelegate.drawerController!.toggleDrawerSide(.left, animated: true, completion: nil)
            
            break;
            
        case 8:
            
            let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
            let centerNav = UINavigationController(rootViewController: centerViewController)
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.drawerController!.centerViewController = centerNav
            appDelegate.drawerController!.toggleDrawerSide(.left, animated: true, completion: nil)
            
            break;
            
        default:
            
            print("\(menuItems[(indexPath as NSIndexPath).row]) is selected");
            
        }
        
    }
    
    
    
}

