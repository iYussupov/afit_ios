//
//  MainMenuWrapStyle.swift
//  AFit
//
//  Created by Dev1 on 12/16/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit

class MainMenuWrapStyle: UIView {
    
    override func awakeFromNib() {

        layer.borderWidth = 1.0
        layer.borderColor = UIColor(red: (255/255.0), green: (255/255.0), blue: (255/255.0), alpha: 0.15).cgColor
        
    }
    
}
