//
//  Schedule.swift
//  AFit
//
//  Created by Dev1 on 3/12/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import Foundation

class Schedule {
    
    fileprivate var _title: String?
    
    fileprivate var _place: String?
    
    fileprivate var _content: String?
    
    fileprivate var _time: String?
    
    fileprivate var _duration: String?
    
    fileprivate var _start_date: String?
    
    fileprivate var _end_date: String?
    
    fileprivate var _weekday: Int?
    
    var _timeStamp: Int?
    
    var timeStamp: Int? {
        return _timeStamp
    }
    
    var weekday: Int? {
        return _weekday
    }
    
    var title: String? {
        return _title
    }
    
    var content: String? {
        return _content
    }
    
    var place: String? {
        return _place
    }
    
    var start_date:String? {
        return _start_date
    }
    
    var end_date:String? {
        return _end_date
    }
    
    var time: String? {
        return _time
    }
    
    var duration: String? {
        return _duration
    }
    
    init (timeStamp: Int?, weekday: Int?, title: String?, content: String?, place: String?, start_date: String?, end_date: String?, time: String?, duration: String?) {
        self._weekday = weekday
        self._title = title
        self._content = content
        self._place = place
        self._time = time
        self._duration = duration
        self._start_date = start_date
        self._end_date = end_date
        self._timeStamp = timeStamp
    }
    
//    init(dictionary: AnyObject) {
//        
//        if let title = dictionary["title"] as? String {
//            self._title = title
//        }
//        
//        if let content = dictionary["content"] as? String {
//            self._content = content
//        }
//        
//        if let place = dictionary["place"] as? String {
//            self._place = place
//        }
//        
//        if let time = dictionary["time"] as? String {
//            self._time = time
//        }
//        
//        if let duration = dictionary["duration"] as? String {
//            self._duration = duration
//        }
//        
//    }
    
}
