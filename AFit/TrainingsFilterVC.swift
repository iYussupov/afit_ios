//
//  TrainingsFilter.swift
//  AFit
//
//  Created by Dev1 on 12/23/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class TrainingsFilterVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var classItems:[String] = [Constants.DEFAULT_FILTER_NAME]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var modalWrapper: UIView!
    @IBOutlet weak var ModalContainer: PopRoundedCorners!
    @IBOutlet weak var modalContaineHeight: NSLayoutConstraint!
    @IBOutlet weak var modalContainerCenter: NSLayoutConstraint!
    
    @IBAction func dismissPopUp(_ sender: Any) {
        
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.modalWrapper.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        UIView.animate(withDuration: 0.5, animations: {
            self.modalWrapper.alpha = 1.0
        })
        
        let screenHeight = UIScreen.main.bounds.height
        let tableViewHeight = tableView.contentSize.height
        var newModalHeight = tableViewHeight - 50
        if newModalHeight > screenHeight - 32 {
            newModalHeight = screenHeight - 32
        }
        if newModalHeight < 300 {
            newModalHeight = 300
        }
        self.modalContaineHeight.constant = newModalHeight

    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.parseData()
        self.checkTimeStamp()
        
    }
    
    func checkTimeStamp() {
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"classes_time_created_\(Constants.URL_SLUG)") != nil {
            
            let timeStampCached = defaults.object(forKey:"classes_time_created_\(Constants.URL_SLUG)") as! Int
            
            Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_CLASSES)/").responseJSON { response in
                
                if let data = response.result.value {
                    
                    let object = JSON(data)
                    
                    let timeStamp = object["time"].int
                    
                    if timeStampCached != timeStamp {
                        
                        CalendarVC.instance?.parseClassesData()
                        
                        self.updateModel(object:object)
                        
                        print("refresh data")
                        
                    }
                    
                } else {
                    
                    print("no internet")
                    
                }
                
            }
            
            print("checking timestamp")
            
        }
        
    }
    
    func parseData() {
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"classes_data_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"classes_data_\(Constants.URL_SLUG)") as! String)
            
            self.updateModel(object:object)
            
            print("cachedData a-ha")
            
        } else {
            
            CalendarVC.instance?.parseClassesData()
            
            if defaults.object(forKey:"classes_data_\(Constants.URL_SLUG)") != nil {
                
                let object = JSON.parse(defaults.object(forKey:"classes_data_\(Constants.URL_SLUG)") as! String)
                
                self.updateModel(object:object)
                
            }
            
        }
    }
    
    func updateModel(object:JSON){
        
        self.classItems = [Constants.DEFAULT_FILTER_NAME]
        
        if object != false {
            
            let classes = object["classes"]
            
            for classObj in classes {
            
                let classItem = classObj.1["name"].string
                
                self.classItems.append(classItem!)
                
                tableView.reloadData()
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let classTitle = self.classItems[(indexPath as NSIndexPath).row]
        
         if let cell = tableView.dequeueReusableCell(withIdentifier: "TrainingsFilterCell") as? TrainingsFilterCell {
        
              cell.trainingTitleLbl.text = classTitle.uppercased()
            
              if Constants.TRAINING_FILTER_NAME == classTitle {
                
                cell.iconUnchecked.alpha = 0.0
                cell.iconChecked.alpha = 1.0
                
              } else {
                
                cell.iconUnchecked.alpha = 1.0
                cell.iconChecked.alpha = 0.0
                
              }
            
              return cell
            
         } else {
            
             return TrainingsFilterCell()
            
         }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let className = classItems[(indexPath as NSIndexPath).row]
        
        Constants.TRAINING_FILTER_NAME = className
        
        CalendarVC.instance?.scheduleDidFiltered()
        
        tableView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
            
        }
        
    }
    
}


