//
//  PhotoGalleryCell.swift
//  AFit
//
//  Created by Dev1 on 3/11/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit

class PhotoGalleryCell: UICollectionViewCell {
    
    @IBOutlet weak var featuredImg: UIImageView!
    
    override func draw(_ rect: CGRect) {
        featuredImg.clipsToBounds = true
    }
    
    
    func configureCell(_ image:UIImage) {
        
        self.featuredImg.image = image
        
    }
        
}
