//
//  ServicesCell.swift
//  AFit
//
//  Created by Dev1 on 3/10/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

class ServicesCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var iconImg: UIImageView!
    
    @IBOutlet weak var thumbImg: UIImageView!
    
    fileprivate var _services: Services?
    
    var services: Services? {
        return _services
    }
    
    
    func configureCell(_ services: Services) {
        
        self._services = services
        
        self.thumbImg.af_cancelImageRequest()
        
        if let serviceName = services.title , serviceName != "" {
            self.titleLbl.text = serviceName.uppercased()
        } else {
            self.titleLbl.text = nil
        }
        
        if let serviceIcon = services.icon , serviceIcon != "" {
            self.iconImg.image = UIImage(named: serviceIcon)
        } else {
            self.iconImg.image = nil
        }
        
        
        if let images = services.featuredImg {
            
            for image in images as! JSON {
                
                if let image_url = image.1["image_url"].string {
                    
                    self.thumbImg.af_setImage(withURL: NSURL(string: image_url) as! URL, placeholderImage: UIImage(named: "mask")!)
                    
                }
                
            }
            
        } else {
            
            self.thumbImg.image = UIImage(named: "mask")
            
        }
        
    }

}
