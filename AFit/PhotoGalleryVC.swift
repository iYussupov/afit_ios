//
//  PhotoGalleryVC.swift
//  AFit
//
//  Created by Dev1 on 3/11/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire
import AlamofireImage
import SwiftyJSON

class PhotoGalleryVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
        
    @IBOutlet weak var collection: UICollectionView!
    
    @IBOutlet weak var slideshow: ImageSlideshow!
    
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate?
    
    var images = [UIImage]()
    
    var lastPosition: Int = -1
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .All
        let value = UIInterfaceOrientation.portraitUpsideDown.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        parseData()
        checkTimeStamp()
            
    }
    
    func checkTimeStamp() {
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"phototour_data_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"phototour_data_\(Constants.URL_SLUG)") as! String)
            
            let timeStampCached = object["time"].int
            
            Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_PHOTO)/").responseJSON { response in
                
                if let data = response.result.value {
                    
                    let object = JSON(data)
                    
                    let timeStamp = object["time"].int
                    
                    if timeStampCached != timeStamp {
                        
                        self.parseFreshData()
                        
                    }
                    
                }
                
            }
            
        }
        
    }
        
        func parseData() {
            
            let defaults = UserDefaults.standard
            
            if defaults.object(forKey:"phototour_data_\(Constants.URL_SLUG)") != nil {
                
                let object = JSON.parse(defaults.object(forKey:"phototour_data_\(Constants.URL_SLUG)") as! String)
                
                let timeStamp = object["time"].int
                
                let featuredImg = object["phototour"]
                
                let phototour = Photo(timeStamp:timeStamp, featuredImg:featuredImg)
                
                self.updateUI(phototour)
                
            } else {
            
                parseFreshData()
                
            }
    
            
            self.collection.reloadData()
        }
    
    func parseFreshData() {
        
        let defaults = UserDefaults.standard
        
        Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_PHOTO)/").responseJSON { response in
            
            if let data = response.result.value {
                
                let object = JSON(data)
                
                defaults.setValue(object.rawString()!, forKey: "phototour_data_\(Constants.URL_SLUG)")
                
                defaults.synchronize()
                
                let timeStamp = object["time"].int
                
                let featuredImg = object["phototour"]
                
                let phototour = Photo(timeStamp:timeStamp, featuredImg:featuredImg)
                
                self.updateUI(phototour)
                
            }
            
        }
    }
    
    func updateUI(_ phototour: Photo) {
        
        self.images = []
        
        let defaults = UserDefaults.standard
        
        for image in phototour.featuredImg as! JSON {
            
            if defaults.object(forKey:"\(image.1)") != nil {
                
                let cachedImg = UIImage(data: defaults.object(forKey:"\(image.1)") as! Data)
                
                self.images.append(cachedImg!)
                
                self.collection.reloadData()
                
                self.slideShowUpdate()
                
            } else {
                
                Alamofire.request("\(image.1)").responseImage { response in
                    
                    if let img = response.result.value {
                        
                        let imageData = UIImageJPEGRepresentation(img, 1.0)
                        
                        defaults.setValue(imageData, forKey: "\(image.1)")
                        
                        defaults.synchronize()
                        
                        self.images.append(img)
                        
                        self.collection.reloadData()
                        
                        self.slideShowUpdate()
                        
                    }
                    
                }
            }
            
        }
        
    }
    
    func slideShowUpdate(){
        
        var imageSet = [InputSource]()
        for item in images {
            let itemSourse = ImageSource(image: item as UIImage)
            imageSet.append(itemSourse)
            
        }
        slideshow.setImageInputs(imageSet)
    }
    
    
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoGalleryCell", for: indexPath) as? PhotoGalleryCell {
               
                let image = images[(indexPath as NSIndexPath).row]
                
                cell.configureCell(image)
                
                let position = (indexPath as NSIndexPath).row
                
                if (position > lastPosition) {
                    
                    let startCellY = cell.frame.origin.y + 50
                    cell.frame.origin.y = startCellY
                    
                    cell.alpha = 0.0
                    
                    UIView.animate(withDuration: 0.35, delay: Double(position) * 0.075, options: [.curveEaseOut], animations: {
                        
                        cell.alpha = 1.0
                        cell.frame.origin.y = startCellY - 50
                        
                    }, completion: nil)
                    
                    lastPosition = position
                    
                }
                
                return cell
            } else {
                return UICollectionViewCell()
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            let ctr = FullScreenSlideshowViewController()
            self.slideshow.setScrollViewPage((indexPath as NSIndexPath).row + 1, animated: false)
            ctr.slideshow.pageControlPosition = PageControlPosition.hidden
            ctr.slideshow.pageControl.currentPageIndicatorTintColor = Constants.ThemeAccentColor
            ctr.initialImageIndex = slideshow.scrollViewPage
            ctr.inputs = slideshow.images
            self.slideshowTransitioningDelegate = ZoomAnimatedTransitioningDelegate(slideshowView: slideshow, slideshowController: ctr)
            ctr.transitioningDelegate = self.slideshowTransitioningDelegate
            self.present(ctr, animated: true, completion: nil)
            
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            return self.images.count
            
        }
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            let screenSize: CGRect = UIScreen.main.bounds
            
            let screenWidth = (screenSize.width - 48)/3 - 2
            
            return CGSize(width: screenWidth, height: screenWidth)
        }
        
        
        @IBAction func openMenu(_ sender: AnyObject) {
            self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
        }
        
    @IBAction func openProfile(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        self.navigationController?.pushViewController(controller, animated: true)
        }
    
    
    
}

