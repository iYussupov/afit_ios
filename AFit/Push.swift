//
//  Push.swift
//  AFit
//
//  Created by Dev1 on 12/19/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import Foundation

class Push {
    
    var _message: String?
    
    var _date: Date?
    
    var _post_id: Int?
    
    var message: String? {
        return _message
    }
    
    var date: Date? {
        return _date
    }
    
    var post_id: Int? {
        return _post_id
    }
    
    init (message: String?, date: Date?, post_id: Int?) {
        self._message = message
        self._date = date
        self._post_id = post_id
    }
}

