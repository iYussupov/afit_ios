//
//  ImageViewBorder.swift
//  AFit
//
//  Created by Dev1 on 12/12/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import Foundation

class ImageViewBorder: UIImageView {
    
    override func awakeFromNib() {
        layer.borderWidth = 1.0
        layer.borderColor = UIColor(red: (255/255.0), green: (255/255.0), blue: (255/255.0), alpha: 0.21).cgColor
        layer.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (0/255.0), alpha: 0.15).cgColor
    }
    
}
