//
//  MenuIconStyle.swift
//  AFit
//
//  Created by Dev1 on 12/16/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

class MenuIconStyle: UIView {
    
    override func awakeFromNib() {
        layer.borderWidth = 2.0
        layer.borderColor = UIColor(red: (255.0/255.0), green: (255.0/255.0), blue: (255.0/255.0), alpha: 0.8).cgColor
        
        layer.cornerRadius = layer.bounds.width / 2
        layer.backgroundColor = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.15).cgColor
        
        
        layer.shadowColor = UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 0.5).cgColor
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 2.0
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        
    }
    
}
