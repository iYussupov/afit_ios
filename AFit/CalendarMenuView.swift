//
//  CalendarMenuView.swift
//

import UIKit

class CalendarMenuView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    var weekDayName = ["MO".localized, "TU".localized, "WE".localized, "TH".localized, "FR".localized, "SA".localized, "SU".localized]
    var padding: CGFloat = 0.0
    var labelFont: UIFont = UIFont(name: "DINPro", size: 14)!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initDayLabel()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initDayLabel() {
        let height = frame.height
        let width = (frame.width  - 1 - padding * 6) / 7
        var rect = CGRect(x: 0, y: 0, width: width, height: height)
        
        for (index, name) in  weekDayName.enumerated() {
            rect.origin.x =  CGFloat(index) * (width + padding)
            let label = UILabel(frame: rect)
            label.text = name
            label.textAlignment = .center
            label.textColor = UIColor.white
            if index == 5 || index == 6 {
                label.textColor = Constants.ThemeYellowColor
            }
            label.font = labelFont
            addSubview(label)
        }
    }
    
    

}
