//
//  ScheduleCell.swift
//  AFit
//
//  Created by Dev1 on 3/12/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit

class ScheduleCell: UITableViewCell {
        
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var placeLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var contentWrap: UIView!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var scheduleActionBtn: UIView!
        
        fileprivate var _schedule: Schedule?
        
        var schedule: Schedule? {
            return _schedule
        }
        
        override func draw(_ rect: CGRect) {
            //        featuredImg.clipsToBounds = true
        }
    
        
        func configureCell(_ schedule: Schedule) {
            
            
            self._schedule = schedule
            
            if let content = schedule.content , content != "" {
                
                self.contentLbl.text = content
                self.contentLbl.setLineHeight()
                
            } else {
                self.contentLbl.text = nil
            }
            
            if let time = schedule.time , time != "" {
                self.timeLbl.text = time.uppercased()
            } else {
                self.timeLbl.text = nil
            }
            
            if let duration = schedule.duration , duration != "" {
                self.durationLbl.text = duration + " " + "MINS".localized.uppercased()
            } else {
                self.durationLbl.text = nil
            }
            
            if let title = schedule.title , title != "" {
                self.titleLbl.text = title.uppercased()
            } else {
                self.titleLbl.text = nil
            }
            
            if let place = schedule.place , place != "" {
                self.placeLbl.text = place.uppercased()
                self.placeLbl.isHidden = false
            } else {
                self.placeLbl.isHidden = true
            }
            
        }
        
}
