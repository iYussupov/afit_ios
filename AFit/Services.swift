//
//  Services.swift
//  AFit
//
//  Created by Dev1 on 3/10/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import Foundation

class Services {
    
    fileprivate var _title: String?
    
    fileprivate var _featuredImg: Any?
    
    fileprivate var _content: String?
    
    fileprivate var _icon: String?
    
    fileprivate var _thumb: String?
    
    fileprivate var _timeStamp: Int?
    
    
    var title: String? {
        return _title
    }
    
    var content: String? {
        return _content
    }
    
    var featuredImg: Any? {
        return _featuredImg
    }
    
    var icon: String? {
        return _icon
    }
    
    var thumb: String? {
        return _thumb
    }
    
    var timeStamp: Int? {
        return _timeStamp
    }
    
    init (timeStamp:Int?, title: String?, content: String?, featuredImg: Any?, icon: String?, thumb: String?) {
        self._title = title
        self._content = content
        self._featuredImg = featuredImg
        self._icon = icon
        self._thumb = thumb
        self._timeStamp = timeStamp
    }
    
//    init(dictionary: AnyObject) {
//        
//        if let title = dictionary["title"] as? String {
//            self._title = title
//        }
//        
//        if let content = dictionary["content"] as? String {
//            self._content = content
//        }
//        
//        if let featuredImg = dictionary["featuredImg"] as? String {
//            self._featuredImg = featuredImg
//        }
//        
//        if let icon = dictionary["icon"] as? String {
//            self._icon = icon
//        }
//        
//        if let thumb = dictionary["thumb"] as? String {
//            self._thumb = thumb
//        }
//        
//    }
    
}
