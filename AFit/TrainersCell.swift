//
//  TrainersCell.swift
//  AFit
//
//  Created by Dev1 on 2/3/17.
//  Copyright © 2017 fxofficeapp. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class TrainersCell: UICollectionViewCell {
    
    @IBOutlet weak var featuredImg: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var featuredImgHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var categoryLbl: UILabel!
    
    
    fileprivate var _trainers: Trainers?
    
    var trainers: Trainers? {
        return _trainers
    }
    
    override func draw(_ rect: CGRect) {
        featuredImg.clipsToBounds = true
    }
    
    func configureCell(_ trainers: Trainers) {
        
        self.featuredImg.af_cancelImageRequest()
        
        self._trainers = trainers
        
        let screenSize: CGRect = UIScreen.main.bounds
        let viewWidth = (screenSize.width - 42)/2
        
        if let url = trainers.featuredImg {
            
            
            self.featuredImg.af_setImage(withURL: NSURL(string: url) as! URL, placeholderImage: UIImage(named: "mask")!)
            

            self.featuredImgHeight.constant = viewWidth
            self.setNeedsLayout()
            self.layoutIfNeeded()
            
        } else {
            
            self.featuredImg.image = UIImage(named: "mask")
            
            
            self.featuredImgHeight.constant = viewWidth
            self.setNeedsLayout()
            self.layoutIfNeeded()
            
        }
        
        if let trainerName = trainers.name , trainerName != "" {
            self.nameLbl.text = trainerName.uppercased()
        } else {
            self.nameLbl.text = nil
        }
        
        if let category = trainers.category , category != "" {
            self.categoryLbl.text = "\(category.uppercased())"
        } else {
//          self.categoryLbl.isHidden = true
            self.categoryLbl.text = " "
        }
        
    }
    
    
}
