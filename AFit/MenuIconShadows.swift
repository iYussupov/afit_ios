//
//  MenuIconShadows.swift
//  AFit
//
//  Created by Dev1 on 12/16/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit

class MenuIconShadows: UIView {
    
    override func awakeFromNib() {
        
        layer.cornerRadius = layer.bounds.width / 2
        layer.backgroundColor = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.03).cgColor

    }
    
}
