//
//  NewsVC.swift
//  AFit
//
//  Created by Ingwar on 3/5/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NewsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
        
        
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var NoNetworkLbl: UIView!
        
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .All
    }
        
    var news = [News]()
    var category: Category!
    static var imageCache = NSCache<AnyObject, AnyObject>()
    let newsLimit = 2
    var newsSkip = 0
    var newsCount = 0
    var refreshControl:UIRefreshControl!
    var loadMoreStatus = false
    var isRefreshing = false
    var preventAnimation = Set<IndexPath>()
    let postsPerPage = 10
    var page = 1
    var viewInitialized: Bool = false
        
    override func viewDidLoad() {
        super.viewDidLoad()
            
          
        self.refreshControl = UIRefreshControl()
        self.refreshControl.tintColor = Constants.ThemeAccentColor
        self.refreshControl.addTarget(self, action: #selector(NewsVC.refresh(_:)), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl)
            
        tableView.dataSource = self
        tableView.delegate = self
            
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.parseFreshData(page: self.page)
        
    }
    
    
    func parseOldData(page:Int){
        
        let defaults = UserDefaults.standard
        
        if page == 1 {
            
            self.NoNetworkLbl.alpha = 1.0
            print("Интернет соединение отсутствует!")
        
        }
        
        if defaults.object(forKey:"posts_data_\(page)_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"posts_data_\(page)_\(Constants.URL_SLUG)") as! String)
            
            self.updateModel(objects:object)
            
            if page == 1 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    
                    self.refreshControl.endRefreshing()
                    self.NoNetworkLbl.alpha = 0.0
                    
                }
            }
            
        } else {
            
            self.NoNetworkLbl.alpha = 1.0
            print("Интернет соединение отсутствует!")
            
        }
    
    }
        
    func parseFreshData(page:Int) {
        
        if page == 1 && !self.isRefreshing {
            self.refreshControl.beginRefreshing()
            self.isRefreshing = true
        }
        
        let defaults = UserDefaults.standard
        
        Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_POSTS)?per_page=\(self.postsPerPage)&page=\(page)").responseJSON { response in
                
            if let data = response.result.value {
                    
                let objects = JSON(data)
                
                defaults.setValue(objects.rawString()!, forKey: "posts_data_\(page)_\(Constants.URL_SLUG)")
                
                defaults.synchronize()
                
                self.updateModel(objects: objects)
                
                
                if page == 1 && self.isRefreshing {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        
                        self.refreshControl.endRefreshing()
                        self.isRefreshing = false
                        
                    }
                }
                
                print("UPDATED")
            } else {
                
                self.parseOldData(page: page)
                
            }
        }
        
    }
    
    func updateModel(objects:JSON){
        
        for (_,object):(String, JSON) in objects {
            
            let timeStamp = object["timestamp"].int
            
            let id = object["id"].int
            
            var title = ""
            
            do {
                title = try object["title"]["rendered"].string!.convertHtmlSymbols()!
            } catch {
                
            }
            
            var content = ""
            
            do {
                content = try object["content"]["rendered"].string!.convertHtmlSymbols()!
            } catch {
                
            }
            
            let featuredImg = object["featured_image_thumbnail_url"].string
            
            let date = object["date"].string
            
            let dateFormatter = DateFormatter()
            
            let createdDate = dateFormatter.dateFromSwapiString(dateString:date!)
            
            let category = object["post_category_name"].string
            
            let new = News(timeStamp:timeStamp, postId:id, title:title, content:content, featuredImg:featuredImg, category:category, date: createdDate)
            
            self.news.append(new)
            
            self.news.sort{ $0.date! > $1.date!}
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
            
        }
        
    }
    
    func refresh(_ sender:AnyObject) {
        self.isRefreshing = true
        refreshBegin("Refresh",
            refreshEnd: {(x:Int) -> () in
                self.refreshControl.endRefreshing()
                self.isRefreshing = false
        })
            
    }
    
    func refreshBegin(_ newtext:String, refreshEnd:@escaping (Int) -> ()) {
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                self.preventAnimation.removeAll()
                self.news.removeAll()
                self.page = 1
                self.parseFreshData(page: self.page)
            }
            sleep(2)
                
            DispatchQueue.main.async {
                refreshEnd(0)
            }
        }
    }
        
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (maximumOffset - currentOffset) <= 200 {
            self.loadMore()
        }
    }
        
    func loadMore() {
        if !loadMoreStatus && !isRefreshing {
            self.loadMoreStatus = true
            loadMoreBegin("Load more",
            loadMoreEnd: {(x:Int) -> () in
                self.loadMoreStatus = false
            })
        }
    }
        
    func loadMoreBegin(_ newtext:String, loadMoreEnd:@escaping (Int) -> ()) {
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                self.page = self.page + 1
                self.parseFreshData(page:self.page)
            }
            sleep(2)
            
            DispatchQueue.main.async {
                loadMoreEnd(0)
            }
        }
    }
        
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        let news = self.news[(indexPath as NSIndexPath).row]
            
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell") as? NewsCell {
                
            cell.newsShareBtn.tag = indexPath.row
                
            cell.configureCell(news)
            
            return cell
            
        } else {
            
            return NewsCell()
            
        }
            
    }
        
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let position = (indexPath as NSIndexPath).row
        
        if !self.viewInitialized  {
            
            let startCellY = cell.frame.origin.y + 100
            cell.frame.origin.y = startCellY
            
            cell.alpha = 0.0
            
            UIView.animate(withDuration: 0.35, delay: Double(position + 1) * 0.075, options: [.curveEaseOut], animations: {
                
                cell.alpha = 1.0
                cell.frame.origin.y = startCellY - 100
                
            }, completion: nil)
            
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.viewInitialized = true
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let news = self.news[(indexPath as NSIndexPath).row]
        performSegue(withIdentifier: "NewsDetailVC", sender: news)
            
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NewsDetailVC" {
            if let newsDetailVC = segue.destination as? NewsDetailVC {
                if let news = sender as? News {
                    newsDetailVC.news = news
                    newsDetailVC.fromPushDirectly = false
                }
            }
        }
    }

    @IBAction func openMenu(_ sender: AnyObject) {
        self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
        
    }
    
    @IBAction func openProfile(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
}

extension DateFormatter {
    func dateFromSwapiString(dateString: String) -> Date? {
        // SWAPI dates look like: "2014-12-10T16:44:31"
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        self.timeZone = TimeZone(abbreviation: "UTC")
        self.locale = Locale(identifier: "en_US_POSIX")
        return self.date(from: dateString)
    }
    func dateFromCustomString(dateString: String) -> Date? {
        self.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.timeZone = TimeZone(abbreviation: "UTC")
        self.locale = Locale(identifier: "en_US_POSIX")
        return self.date(from: dateString)
    }
}
