//
//  NavigationBarStyle.swift
//  AFit
//
//  Created by Dev1 on 8/25/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//
import UIKit

class NavigationBarStyle: UIView {
    
    override func awakeFromNib() {
        layer.backgroundColor = Constants.ThemeMainColor.cgColor
    }
    
}
