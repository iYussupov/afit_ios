//
//  YellowLabelColor.swift
//  AFit
//
//  Created by Dev1 on 8/25/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//
import UIKit

class YellowLabelColor: UILabel {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
//        self.layer.cornerRadius = self.bounds.width/2
//        self.clipsToBounds = true
        self.textColor = Constants.ThemeYellowColor
//        self.setProperties(1.0, borderColor:UIColor.blackColor())
    }
    
//    func setProperties(borderWidth: Float, borderColor: UIColor) {
//        self.layer.borderWidth = CGFloat(borderWidth)
//        self.layer.borderColor = borderColor.CGColor
//    }
    
}