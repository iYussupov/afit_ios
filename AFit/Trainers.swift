//
//  Trainers.swift
//  AFit
//
//  Created by Dev1 on 3/10/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import Foundation

class Trainers {
    
    var _timeStamp: Int?
    
    var _name: String?
    
    var _featuredImg: String?
    
    var _category: String?
    
    var _content: String?
    
    var timeStamp: Int? {
        return _timeStamp
    }
    
    var name: String? {
        return _name
    }
    
    var featuredImg: String? {
        return _featuredImg
    }
    
    var category: String? {
        return _category
    }
    
    var content: String? {
        return _content
    }
    
    init (timeStamp: Int?, name: String?, featuredImg: String?, category: String?, content: String?) {
        self._timeStamp = timeStamp
        self._name = name
        self._category = category
        self._featuredImg = featuredImg
        self._content = content
    }
    
}
