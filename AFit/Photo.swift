//
//  Photo.swift
//  AFit
//
//  Created by Dev1 on 12/8/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import Foundation

class Photo {
    
    var _timeStamp: Int?
    
    var _featuredImg: Any?
    
    var timeStamp: Int? {
        return _timeStamp
    }
    
    var featuredImg: Any? {
        return _featuredImg
    }
    
    init (timeStamp: Int?, featuredImg: Any?) {
        self._featuredImg = featuredImg
        self._timeStamp = timeStamp
    }
}
