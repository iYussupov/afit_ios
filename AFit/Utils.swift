//
//  Utils.swift
//  AFit
//
//  Created by Dev1 on 12/27/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import Foundation

struct Utils {
    
    func sendMail(_ sender: UITapGestureRecognizer) {
        
        if let client_name = self.nameField.text, !client_name.isEmpty, let client_phone = self.phoneField.text, !client_phone.isEmpty {
            
            let parameters: Parameters = ["client_phone": client_phone,"client_name": client_name, "training_name": self.trainingName.text!,
                                          "training_time": self.trainingTime.text!,"training_date": self.signUpDate.text!]
            
            Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_MAILER)/", parameters: parameters).responseString { response in
                
                if response.result.value != nil {
                    
                    self.showDialog(title:"Все ок", message: "Вы успешно записались на занятие \(self.schedule.title!)")
                    
                    
                    self.removeFromParentViewController()
                    self.view.removeFromSuperview()
                    
                } else {
                    
                    print("signup error")
                    
                }
                
            }
            
        } else {
            
            showDialog(title:"Ошибка!", message: "Заполните все поля")
            
        }
        
    }
    
    func showDialog(title:String, message: String){
        
        
        let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
    }

}
