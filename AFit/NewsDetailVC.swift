//
//  NewsDetailVC.swift
//  AFit
//
//  Created by Ingwar on 3/6/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import FBSDKShareKit
import VK_ios_sdk

let offset_HeaderStop:CGFloat = 243.0 // At this offset the Header stops its transformations

class NewsDetailVC: UIViewController, UIScrollViewDelegate  {
    
        var news: News!
        var fromPushDirectly:Bool?
        var toggleRightDrawer: Bool?
//        static var imageCache = NSCache()
        var preventAnimation = Set<IndexPath>()
    
        var shareTrigger = false
    
        @IBOutlet weak var headerTitle: UILabel!
        @IBOutlet weak var headerImage: UIImageView!
        @IBOutlet var scrollView:UIScrollView!
        @IBOutlet weak var postDate: UILabel!
        @IBOutlet weak var titleLbl: UILabel!
        @IBOutlet weak var contentField: UILabel!
    
        @IBOutlet weak var imageViewer: UIView!
        
        @IBOutlet weak var categoryLbl: UILabel!
        @IBOutlet weak var backBtnView: UIButton!
    @IBOutlet weak var facebookShareBtnView: RoundBtnViewStyle!
    @IBOutlet weak var vkShareBtnView: RoundBtnViewStyle!
        
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.isNavigationBarHidden = true
            self.evo_drawerController?.openDrawerGestureModeMask = .Custom
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            scrollView.delegate = self
            self.updateUI()
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(NewsDetailVC.showImageViewer))
            imageViewer.addGestureRecognizer(tap)
            
        }
        
        func showImageViewer() {
            let img = news.featuredImg
            performSegue(withIdentifier: "ViewerVC", sender: img)
        }
    
        
        
        func updateUI() {
            
            let Date:DateFormatter = DateFormatter()
            Date.dateFormat = "dd.MM.yyyy"
            postDate.text = Date.string(from: news.date!)
            
            titleLbl.text = news.title?.uppercased()
            headerTitle.text = news.title?.uppercased()
            
            self.contentField.text = news.content
            self.contentField.setLineHeight()
            
            if let category = news.category , category != "" {
                self.categoryLbl.text = "\(category.uppercased())"
                self.categoryLbl.backgroundColor = UIColor(red: 118.0/255.0, green: 190.0/255.0, blue: 52.0/255.0, alpha: 1.0)
                self.categoryLbl.layer.borderColor = UIColor(red: (118.0/255.0), green: (190.0/255.0), blue: (52.0/255.0), alpha: 1.0).cgColor
            } else {
                self.categoryLbl.isHidden = true
            }
            
            if let url = news.featuredImg {
                
                self.headerImage.af_setImage(withURL: NSURL(string: url) as! URL, placeholderImage: UIImage(named: "mask")!)
                
            } else {
                
                self.headerImage.image = UIImage(named: "mask")
                
            }
            
        }
        
        override func viewDidAppear(_ animated: Bool) {
            
            headerImage.clipsToBounds = true
            
        }
    
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            
            let offset = scrollView.contentOffset.y
            var headerTransform = CATransform3DIdentity
            var labelsTransform = CATransform3DIdentity
            
            // PULL DOWN -----------------
            
            if offset < 0 {
                
                let headerScaleFactor:CGFloat = -(offset) / headerImage.bounds.height
                let headerSizevariation = ((headerImage.bounds.height * (1.0 + headerScaleFactor)) - headerImage.bounds.height)/2.0
                headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
                headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
                
                labelsTransform = CATransform3DTranslate(labelsTransform, 0, offset, 0)
                
                headerImage.layer.transform = headerTransform
                categoryLbl.layer.transform = labelsTransform
            }
                
                // SCROLL UP/DOWN ------------
                
            else {
                
                // Header -----------
                
                headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
                labelsTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
                
            }
            
            // Apply Transformations
            
            headerImage.layer.transform = headerTransform
        }
    
    
    
    
    
    @IBOutlet weak var postShareHover: RoundBtnViewStyle!
    
    @IBAction func postShareDown(_ sender: AnyObject) {
//        self.postShareHover.backgroundColor = UIColor(red: 136/255, green: 38/255, blue: 147/255, alpha: 1)
    }
    
    @IBAction func postShareAction(_ sender: AnyObject) {
        
        if self.shareTrigger == false {
            
            
            UIView.animate(withDuration: 0.25, animations:{
                
                self.postShareHover.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
                
            })
            
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseInOut], animations: {
                
                self.facebookShareBtnView.transform = CGAffineTransform(translationX: -123.0, y: 0.0)
                
            }, completion: nil)
            
            UIView.animate(withDuration: 0.25, delay: 0.05, options: [.curveEaseInOut], animations: {
                
                self.vkShareBtnView.transform = CGAffineTransform(translationX: -62.0, y: 0.0)
                
            }, completion: nil)
            
            self.shareTrigger = true
        
        
        } else {
            
            
            UIView.animate(withDuration: 0.25, animations:{
                
                self.postShareHover.transform = CGAffineTransform(rotationAngle: CGFloat(0.0))
                
            })
            
            UIView.animate(withDuration: 0.25, delay: 0.05, options: [.curveEaseInOut], animations: {
                
                self.facebookShareBtnView.transform = CGAffineTransform(translationX: 0.0, y: 0.0)
                
            }, completion: nil)
            
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseInOut], animations: {
                
                self.vkShareBtnView.transform = CGAffineTransform(translationX: 0.0, y: 0.0)
                
            }, completion: nil)
            
            self.shareTrigger = false
        
        
        }
        
    }
    
    @IBAction func vkShareBtnAction(_ sender: Any) {
        
        var titleToShare = ""
        var contentToShare = ""
        
        if news!.title != nil {
            titleToShare = "\(news!.title!)"
        }
        
        if news!.content != nil {
            contentToShare = "\(news!.content!)"
        }
        
        let shareDialog = VKShareDialogController()
        shareDialog.text = "\(titleToShare.uppercased())\n\(contentToShare)"
        shareDialog.uploadImages = [VKUploadImage(image: self.headerImage.image, andParams: VKImageParameters.jpegImage(withQuality: 1.0)!)]
        shareDialog.shareLink = VKShareLink(title: "", link: URL(string:Constants.webSiteUrl))
        
        shareDialog.completionHandler = { result in
            self.dismiss(animated: true, completion: nil)
        }
        
        self.present(shareDialog, animated: true, completion: nil)
        
    }
    
    @IBAction func facebookShareBtnAction(_ sender: Any) {
        
        var titleToShare = ""
        var contentToShare = ""
        var imageToShare = ""
        
        if news!.title != nil {
            titleToShare = "\(news!.title!)"
        }
        
        if news!.content != nil {
            contentToShare = "\(news!.content!)"
        }
        
        if news!.featuredImg != nil {
            imageToShare = "\(news!.featuredImg!)"
        }
        
        let content = FBSDKShareLinkContent()
        content.contentURL = URL(string:Constants.webSiteUrl)
        content.contentDescription = contentToShare
        content.contentTitle = titleToShare
        content.imageURL = URL(string:imageToShare)
        
        let dialog = FBSDKShareDialog()
        dialog.fromViewController = self
        dialog.shareContent = content
        dialog.mode = FBSDKShareDialogMode.native
        dialog.show()
        
    }
    
        @IBAction func DetailsBackBtn(_ sender: AnyObject) {

            if fromPushDirectly! {
                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "PushVC") as! PushVC
                let centerNav = UINavigationController(rootViewController: centerViewController)
                
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.drawerController!.centerViewController = centerNav
                
            } else {
                if toggleRightDrawer != true {
                    let _ = self.navigationController?.popViewController(animated: true)
                } else {
                    let _ = self.navigationController?.popViewController(animated: true)
                }
            }
            
        }
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "ViewerVC" {
                if let viewerVC = segue.destination as? ViewerVC {
                    if let img = news.featuredImg {
                        viewerVC.img = img
                    }
                }
            }
        }

    @IBAction func openProfile(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        self.navigationController?.pushViewController(controller, animated: true)
        }
    
        
}

