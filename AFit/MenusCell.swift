//
//  MenusCell.swift
//  AFit
//
//  Created by Dev1 on 11/28/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit

class MenusCell: UICollectionViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var iconLbl: UILabel!
    @IBOutlet weak var PanelView: PanelsViewBorder!
    @IBOutlet weak var cellContentWrapper: MainMenuWrapStyle!
    
    fileprivate var _menus: Menus?
    
    var menus: Menus? {
        return _menus
    }
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame = newFrame
            frame.origin.y += 8
            super.frame = frame
        }
    }
    
    func panelHover(){
    
        self.PanelView.backgroundColor = UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 0.4)
        
    }
    
    func configureCell(_ menus: Menus) {
        
        self._menus = menus
        
        if let title = menus.title , title != "" {
            
            self.titleLbl.text = title
            
        }
        
        if let icon = menus.icon , icon != "" {
            
            self.iconLbl.text = icon
            
        }
        

        
    }
    
}
