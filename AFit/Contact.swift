//
//  Contact.swift
//  AFit
//
//  Created by Dev1 on 12/12/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import Foundation

class Contact {
    
    var _timeStamp: Int?
    
    var _title: String?
    
    var _address: String?
    
    var _lat: Double?
    
    var _lng: Double?
    
    var _zoom: Float?
    
    var _depOneTitle: String?
    
    var _depOnePhoneOne: String?
    
    var _depOnePhoneTwo: String?
    
    var _depTwoTitle: String?
    
    var _depTwoPhoneOne: String?
    
    var _depTwoPhoneTwo: String?
    
    var _mainPhoneNumber: String?
    
    var _workDay: String?
    
    var _weekEnd: String?
    
    var _depOneEmail: String?
    
    var _depTwoEmail: String?
    
    
    var timeStamp: Int? {
        return _timeStamp
    }
    
    var title: String? {
        return _title
    }
    
    var address: String? {
        return _address
    }
    
    var lat: Double? {
        return _lat
    }
    
    var lng: Double? {
        return _lng
    }
    
    var zoom: Float? {
        return _zoom
    }
    
    var depOneTitle: String? {
        return _depOneTitle
    }
    
    var depOnePhoneOne: String? {
        return _depOnePhoneOne
    }
    
    var depOnePhoneTwo: String? {
        return _depOnePhoneTwo
    }
    
    var depTwoTitle: String? {
        return _depTwoTitle
    }
    
    var depTwoPhoneOne: String? {
        return _depTwoPhoneOne
    }
    
    var depTwoPhoneTwo: String? {
        return _depTwoPhoneTwo
    }
    
    var mainPhoneNumber: String? {
        return _mainPhoneNumber
    }
    
    var workDay: String? {
        return _workDay
    }
    
    var weekEnd: String? {
        return _weekEnd
    }
    
    var depOneEmail: String? {
        return _depOneEmail
    }
    
    var depTwoEmail: String? {
        return _depTwoEmail
    }
    
    init (timeStamp: Int?, title: String?, address: String?, lat: Double?, lng: Double?, zoom: Float?, depOneTitle: String?, depOnePhoneOne: String?, depOnePhoneTwo: String?, depTwoTitle: String?, depTwoPhoneOne: String?, depTwoPhoneTwo: String?, mainPhoneNumber: String?, workDay: String?, weekEnd: String?, depOneEmail:String?, depTwoEmail:String? ) {
        
        self._title = title
        self._address = address
        self._timeStamp = timeStamp
        self._lat = lat
        self._lng = lng
        self._zoom = zoom
        self._depOneTitle = depOneTitle
        self._depOnePhoneOne = depOnePhoneOne
        self._depOnePhoneTwo = depOnePhoneTwo
        self._depTwoTitle = depTwoTitle
        self._depTwoPhoneOne = depTwoPhoneOne
        self._depTwoPhoneTwo = depTwoPhoneTwo
        self._mainPhoneNumber = mainPhoneNumber
        self._workDay = workDay
        self._weekEnd = weekEnd
        self._depOneEmail = depOneEmail
        self._depTwoEmail = depTwoEmail
        
    }
}

