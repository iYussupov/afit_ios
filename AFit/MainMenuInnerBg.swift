//
//  MainMenuInnerBg.swift
//  AFit
//
//  Created by Dev1 on 12/16/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

class MainMenuInnerBg: UIView {
    
    override func awakeFromNib() {
        
        let subLayer = CAGradientLayer()
        subLayer.frame = CGRect(x: 0, y: 0, width: layer.bounds.width, height: layer.bounds.height)
        
        let startColor = Constants.ThemeMenuStartColor.cgColor
        let endColor = Constants.ThemeEndStartColor.cgColor
        
        subLayer.colors = [startColor, endColor]
        layer.addSublayer(subLayer)
        
    }
    
}
