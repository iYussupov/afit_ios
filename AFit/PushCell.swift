//
//  PushCell.swift
//  AFit
//
//  Created by Dev1 on 12/19/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.

import UIKit

class PushCell: UITableViewCell {
    
    @IBOutlet weak var messageLbl: UILabel!
    
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var pushActionBtn: UILabel!
    
    @IBOutlet weak var divider: UIView!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    fileprivate var _push: Push?
    
    var push: Push? {
        return _push
    }
    
    
    func configureCell(_ push: Push) {
        
        self._push = push
        
        if let message = push.message , message != "" {
            self.messageLbl.text = message.uppercased()
        } else {
            self.messageLbl.text = nil
        }
        
        
        if let post_id = push.post_id , post_id != 0 {
            
            self.pushActionBtn.isHidden = false
            self.topConstraint.constant = 13.0
            self.layoutIfNeeded()
            
        } else {
            
            self.pushActionBtn.isHidden = true
            self.topConstraint.constant = -8.0
            self.layoutIfNeeded()
            
        }
        
        if let date = push.date {
            
            let Date:DateFormatter = DateFormatter()
            Date.dateFormat = "dd.MM.yyyy HH:mm"
            Date.timeZone = TimeZone(abbreviation: "UTC")
            Date.locale = Locale(identifier: "en_US_POSIX")
            
            self.dateLbl.text = Date.string(from: date)
            
        }
        
    }
    
    
    
    
}

