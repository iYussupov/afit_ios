//
//  ContactsVC.swift
//  AFit
//
//  Created by Dev1 on 3/11/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class ContactsVC: UIViewController, UIScrollViewDelegate {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var callBtnView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var workDayLbl: UILabel!
    @IBOutlet weak var weekEndLbl: UILabel!
    @IBOutlet weak var depOneTitle: UILabel!
    @IBOutlet weak var depOnePhoneOne: UILabel!
    @IBOutlet weak var depOnePhoneTwo: UILabel!
    @IBOutlet weak var depTwoTitle: UILabel!
    @IBOutlet weak var depTwoPhoneOne: UILabel!
    @IBOutlet weak var depTwoPhoneTwo: UILabel!
    @IBOutlet weak var depOnePhoneIcon: UIImageView!
    @IBOutlet weak var depTwoPhoneIcon: UIImageView!
    @IBOutlet weak var mainPhoneLbl: UILabel!
    @IBOutlet weak var depOneEmailLbl: YellowLabelColor!
    @IBOutlet weak var depTwoEmailLbl: YellowLabelColor!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .All
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        
        self.parseData()
        self.checkTimeStamp()
        
    }
    
    func mapInit(_ contact: Contact){
        
        if let lng = contact.lng, let lat = contact.lat, let zoom = contact.zoom {
            
            self.mapView.clear()
            
            let camera = GMSCameraPosition.camera(withLatitude: lng,longitude: lat, zoom: zoom)
            self.mapView.camera = camera
            
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(lng,lat)
            marker.title = Constants.APP_FAMILY
            marker.icon = UIImage(named: "map_icon")
            marker.map = self.mapView
            
        }
        
    }
    
    func checkTimeStamp() {
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"contact_data_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"contact_data_\(Constants.URL_SLUG)") as! String)
            
            let timeStampCached = object["time"].int
            
            Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_CONTACTS)/").responseJSON { response in
                
                if let data = response.result.value {
                    
                    let object = JSON(data)
                    
                    let timeStamp = object["time"].int
                    
                    if timeStampCached != timeStamp {
                        
                        self.parseFreshData()
                        
                        print("refresh data")
                        
                    }
                    
                } else {
                    
                    print("no internet")
                    
                }
                
            }
            
            print("checking timestamp")
            
        }
        
    }
    
    func parseData() {
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"contact_data_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"contact_data_\(Constants.URL_SLUG)") as! String)
            
            self.updateModel(object:object)
            
            print("cachedData a-ha")
            
        } else {
            
            self.parseFreshData()
            
        }
    }
    
    func parseFreshData() {
        
        let defaults = UserDefaults.standard
        
        Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_CONTACTS)/").responseJSON { response in
            
            if let data = response.result.value {
                
                let object = JSON(data)
                
                defaults.setValue(object.rawString()!, forKey: "contact_data_\(Constants.URL_SLUG)")
                
                defaults.synchronize()
                
                self.updateModel(object:object)
                
            }
            
        }
    }
    
    func updateModel(object:JSON){
        
        let timeStamp = object["time"].int
        
        let title = object["title_club"].string
        
        let address = object["adress"].string
        
        var lat:Double = 0.0
        
        var lng:Double = 0.0
        
        var zoom:Float = 0
        
        if Double(object["lat"].string!) != nil {
            lat = Double(object["lat"].string!)!
        }
        
        if Double(object["lng"].string!) != nil {
            lng = Double(object["lng"].string!)!
        }
        
        if Float(object["zoom"].string!) != nil {
            zoom = Float(object["zoom"].string!)!
        }
        
        let depOneTitle = object["department"][0][0].string
        
        let depOnePhoneOne = object["department"][0][1].string
        
        let depOnePhoneTwo = object["department"][0][2].string
        
        let depOneEmail = object["department"][0][3].string
        
        let depTwoTitle = object["department"][1][0].string
        
        let depTwoPhoneOne = object["department"][1][1].string
        
        let depTwoPhoneTwo = object["department"][1][2].string
        
        let depTwoEmail = object["department"][1][3].string
        
        let mainPhoneNumber = object["main_phone"].string
        
        let workDay = object["work_day"][0][0].string
        
        let weekEnd = object["work_day"][1][0].string
        
        let contact_data = Contact(timeStamp:timeStamp,title:title, address:address, lat:lat, lng: lng, zoom: zoom, depOneTitle: depOneTitle, depOnePhoneOne: depOnePhoneOne, depOnePhoneTwo: depOnePhoneTwo, depTwoTitle: depTwoTitle, depTwoPhoneOne: depTwoPhoneOne, depTwoPhoneTwo: depTwoPhoneTwo, mainPhoneNumber: mainPhoneNumber, workDay: workDay, weekEnd: weekEnd, depOneEmail:depOneEmail, depTwoEmail:depTwoEmail )
        
        self.updateUI(contact_data)
    }
    
    func updateUI(_ contact: Contact) {
        
        
        if let contactTitle = contact.title, contactTitle != "" {
            self.titleLbl.text = contactTitle.uppercased()
            self.titleLbl.isHidden = false
        } else {
            self.titleLbl.isHidden = true
        }
        
        if let addressLbl = contact.address, addressLbl != "" {
            self.addressLbl.text = addressLbl
            self.addressLbl.setLineHeight()
            self.addressLbl.isHidden = false
        } else {
            self.addressLbl.isHidden = true
        }
        
        if let workDay = contact.workDay, workDay != "" {
            self.workDayLbl.text = workDay.uppercased()
            self.workDayLbl.isHidden = false
        } else {
            self.workDayLbl.isHidden = true
        }
        
        if let weekEnd = contact.weekEnd, weekEnd != "" {
            self.weekEndLbl.text = weekEnd.uppercased()
            self.weekEndLbl.isHidden = false
        } else {
            self.weekEndLbl.isHidden = true
        }
        
        if let depOneTitle = contact.depOneTitle, depOneTitle != "" {
            self.depOneTitle.text = depOneTitle.uppercased()
            self.depOneTitle.isHidden = false
        } else {
            self.depOneTitle.isHidden = true
        }
        
        if let depOnePhoneOne = contact.depOnePhoneOne, depOnePhoneOne != "" {
            self.depOnePhoneOne.text = depOnePhoneOne
            self.depOnePhoneOne.isHidden = false
            self.depOnePhoneIcon.isHidden = false
        } else {
            self.depOnePhoneOne.isHidden = true
            self.depOnePhoneIcon.isHidden = true
        }
        
        if let depOnePhoneTwo = contact.depOnePhoneTwo, depOnePhoneTwo != "" {
            self.depOnePhoneTwo.text = depOnePhoneTwo
            self.depOnePhoneTwo.isHidden = false
        } else {
            self.depOnePhoneTwo.isHidden = true
        }
        
        if let depTwoTitle = contact.depTwoTitle, depTwoTitle != "" {
            self.depTwoTitle.text = depTwoTitle.uppercased()
            self.depTwoTitle.isHidden = false
        } else {
            self.depTwoTitle.isHidden = true
        }
        
        if let depTwoPhoneOne = contact.depTwoPhoneOne, depTwoPhoneOne != "" {
            self.depTwoPhoneOne.text = depTwoPhoneOne
            self.depTwoPhoneOne.isHidden = false
            self.depTwoPhoneIcon.isHidden = false
        } else {
            self.depTwoPhoneOne.isHidden = true
            self.depTwoPhoneIcon.isHidden = true
        }
        
        if let depTwoPhoneTwo = contact.depTwoPhoneTwo, depTwoPhoneTwo != "" {
            self.depTwoPhoneTwo.text = depTwoPhoneTwo
            self.depTwoPhoneTwo.isHidden = false
        } else {
            self.depTwoPhoneTwo.isHidden = true
        }
        
        if let mainPhoneNumber = contact.mainPhoneNumber, mainPhoneNumber != "" {
            self.mainPhoneLbl.text = mainPhoneNumber
            self.mainPhoneLbl.isHidden = false
            self.callBtnView.isHidden = false
        } else {
            self.mainPhoneLbl.isHidden = true
            self.callBtnView.isHidden = true
        }
        
        if let depOneEmail = contact.depOneEmail, depOneEmail != "" {
            self.depOneEmailLbl.text = depOneEmail
            self.depOneEmailLbl.isHidden = false
        } else {
            self.depOneEmailLbl.isHidden = true
        }
        
        if let depTwoEmail = contact.depTwoEmail, depTwoEmail != "" {
            self.depTwoEmailLbl.text = depTwoEmail
            self.depTwoEmailLbl.isHidden = false
        } else {
            self.depTwoEmailLbl.isHidden = true
        }
        
        
        self.mapInit(contact)
        
        let tap = ContactTapGestureRecognizer(target: self, action: #selector(ContactsVC.callAdminNumber))
        tap.contact = contact
        self.callBtnView.addGestureRecognizer(tap)
        
    }
    
    
    func callAdminNumber(gestureRecognizer: ContactTapGestureRecognizer) {
        
        if let contact = gestureRecognizer.contact {
            let number = removeSpecialCharsFromString(text: contact.mainPhoneNumber!)
            if let url = NSURL(string: "tel://\(number)") {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("1234567890+".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
    @IBAction func openMenu(_ sender: AnyObject) {
        self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
        
    }
    
    @IBAction func openProfile(_ sender: AnyObject) {
//        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        var headerTransform = CATransform3DIdentity
        
        // PULL DOWN -----------------
        let headerScaleFactor:CGFloat = -(offset) / mapContainerView.bounds.height
        
        if offset < 0 {
            
            
            let headerSizevariation = ((mapContainerView.bounds.height * (1.0 + headerScaleFactor)) - mapContainerView.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation + offset, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            
            
            mapContainerView.layer.transform = headerTransform
        }
            
            // SCROLL UP/DOWN ------------
            
        else {
            
            // Header -----------
            
            headerTransform = CATransform3DTranslate(headerTransform, 0, 0, 0)
            
        }
        
        // Apply Transformations
        
        mapContainerView.layer.transform = headerTransform
    }
    
}

class ContactTapGestureRecognizer: UITapGestureRecognizer {
    var contact: Contact?
}
