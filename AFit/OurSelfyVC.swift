//
//  OurSelfyVC.swift
//  AFit
//
//  Created by Dev1 on 3/11/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit

class OurSelfyVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
        
        @IBOutlet weak var collection: UICollectionView!
        
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.isNavigationBarHidden = true
            self.evo_drawerController?.openDrawerGestureModeMask = .All
        }
        
        var selfy = [Selfy]()
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
            parseData()
        }
        
        func parseData() {
            
                    let objects = [[
                        "featuredImg":"service_thumb_1",
                        "name":"МАКС КАВАЛЕРА",
                        "likes_count":120,
                        "likes":true,
                        "comments_count":10
                    ],[
                        "featuredImg":"service_thumb_2",
                        "name":"АНДРЕЙ БОРОДИН",
                        "likes_count":90,
                        "comments_count":17
                    ],[
                        "featuredImg":"service_thumb_3",
                        "name":"АЛЕНА БОРОДИНА",
                        "likes_count":10,
                        "likes":true,
                        "comments_count":32
                    ],[
                        "featuredImg":"service_thumb_4",
                        "name":"ДЖИГАН",
                        "likes_count":20,
                        "comments_count":18
                    ],[
                        "featuredImg":"service_thumb_5",
                        "name":"КИРИЛЛ ПОТАПОВ",
                        "likes_count":78,
                        "comments_count":14
                    ],[
                        "featuredImg":"service_thumb_6",
                        "name":"АЛЕНА БОРОДИНА",
                        "likes_count":90,
                        "likes":true,
                        "comments_count":1
                    ]]
            
            for object in objects {
                
                let selfi = Selfy(dictionary: object as AnyObject)
                
                self.selfy.append(selfi)
                
            }
            
            self.collection.reloadData()
        }
    
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let selfy = self.selfy[(indexPath as NSIndexPath).row]
            
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OurSelfyCell", for: indexPath) as? OurSelfyCell {
                
                
                cell.configureCell(selfy)
                
                return cell
            } else {
                return UICollectionViewCell()
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            let selfy = self.selfy[(indexPath as NSIndexPath).row]
            performSegue(withIdentifier: "OurSelfyDetailVC", sender: selfy)
            
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            return self.selfy.count
            
        }
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            let screenSize: CGRect = UIScreen.main.bounds
            
            let screenWidth = (screenSize.width - 40)/2 - 1
            
            return CGSize(width: screenWidth, height: screenWidth)
        }
        
        
        @IBAction func openMenu(_ sender: AnyObject) {
            self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
        }
        
    @IBAction func openProfile(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        self.navigationController?.pushViewController(controller, animated: true)
        }
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "OurSelfyDetailVC" {
                if let detailVC = segue.destination as? OurSelfyDetailVC {
                    if let selfy = sender as? Selfy {
                        detailVC.selfy = selfy
                    }
                }
            }
        }
        
        
}
