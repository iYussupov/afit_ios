//
//  LabelAccentColor.swift
//  AFit
//
//  Created by Dev1 on 8/25/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//
import UIKit

class LabelAccentColor: UILabel {
    
    func setup() {
        self.textColor = Constants.ThemeGreenColor
    }
    
}
