//
//  CalendarDayView.swift
//

import UIKit
protocol CalendarDayViewDelegate {
    func selectedDay(_ dayView: CalendarDayView)
    func unSelectedDay(_ dayView: CalendarDayView)
    func tapOnDay()
}

class CalendarDayView: UIView {
    
    var delegate: CalendarDayViewDelegate?
    
    var dateLabel: UILabel?
    
    var innerBorder:UIView?
    var bottomBorder:UIView?
    
    var date: GDate?
    var isSelectedDay: Bool = false {
        didSet {
            if isSelectedDay {
                setSelectedDay()
            } else {
                unSetSelectedDay()
            }
        }
    }
    var isPresentDay: Bool = false {
        didSet {
            if isPresentDay {
                setPresentDay()
            } else {
                unSetPresentDay()
            }
        }
    }
    
    
    var selectedDayFillColor: UIColor = Constants.ThemeYellowColor
    var presentDayFillColor: UIColor = Constants.ThemeYellowColor
    
    var normalDayFontColor: UIColor = UIColor.white
    var presentDayFontColor: UIColor = Constants.ThemeYellowColor
    var selectedDayFontColor: UIColor = Constants.ThemeYellowColor
    
    var normalDayFont: UIFont = UIFont(name: "DINPro", size: 14)!
    var presentDayFont: UIFont = UIFont(name: "DINPro", size: 14)!
    var selectedDayFont: UIFont = UIFont(name: "DINPro", size: 14)!

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    init(frame: CGRect, date: GDate) {
        super.init(frame: frame)
        self.date = date
        let rect = CGRect(x: 0, y: 0, width: frame.width - 4, height: frame.height)
        dateLabel = UILabel(frame: rect)
        dateLabel!.textColor = normalDayFontColor
        dateLabel!.textAlignment = .center
        dateLabel!.text = "\(date.getDay().day)"
        dateLabel!.font = normalDayFont
        dateLabel!.layer.zPosition = 2
        addSubview(dateLabel!)

        
        innerBorder = UIView(frame: CGRect(x: dateLabel!.frame.width / 2 - 17, y: 0, width: 34, height: 30))
        innerBorder!.layer.borderWidth = 1.0
        innerBorder!.layer.borderColor = UIColor(red: (97/255.0), green: (97/255.0), blue: (97/255.0), alpha: 1.0).cgColor
        innerBorder!.backgroundColor = UIColor(red: (97/255.0), green: (97/255.0), blue: (97/255.0), alpha: 0.1)
        innerBorder!.layer.zPosition = 1
        innerBorder!.isHidden = true
        dateLabel!.addSubview(innerBorder!)
        
        
        bottomBorder = UIView(frame: CGRect(x: dateLabel!.frame.width / 2 - 17, y: dateLabel!.frame.height - 2, width: 34, height: 2))
        bottomBorder!.backgroundColor = Constants.ThemeGreenColor
        bottomBorder!.layer.zPosition = 2
        bottomBorder!.isHidden = true
        dateLabel!.addSubview(bottomBorder!)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CalendarDayView.selectDay(_:)))
        tapGesture.numberOfTapsRequired = 1
        self.addGestureRecognizer(tapGesture)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func selectDay(_ sender: UITapGestureRecognizer) {
        if isSelectedDay {
//            unSetSelectedDay()
//            isSelectedDay = false
//            delegate?.unSelectedDay(self)
        } else {
            setSelectedDay()
            isSelectedDay = true
//            delegate?.selectedDay(self)
//            print(date!.description)
            delegate?.tapOnDay()
            
        }
    }
    
    func setPresentDay() {
        if isSelectedDay {
            dateLabel!.textColor = selectedDayFontColor
            dateLabel!.font = selectedDayFont
        } else {
            dateLabel!.textColor = presentDayFontColor
            dateLabel!.font = presentDayFont
        }
    }
    
    func unSetPresentDay() {
        if isSelectedDay {
            dateLabel!.textColor = selectedDayFontColor
            dateLabel!.font = selectedDayFont
        } else {
            dateLabel!.textColor = normalDayFontColor
            dateLabel!.font = normalDayFont
        }
    }
    
    func setSelectedDay() {
 
        dateLabel!.textColor = selectedDayFontColor
        dateLabel!.font = selectedDayFont
        innerBorder!.isHidden = false
        bottomBorder!.isHidden = false
        delegate?.selectedDay(self)
    }
    
    func unSetSelectedDay() {
        if isPresentDay {
            dateLabel!.textColor = presentDayFontColor
            dateLabel!.font = presentDayFont
            innerBorder!.isHidden = true
            bottomBorder!.isHidden = true
        } else {
            dateLabel!.textColor = normalDayFontColor
            dateLabel!.font = normalDayFont
            innerBorder!.isHidden = true
            bottomBorder!.isHidden = true
        }        
    }
    
}
