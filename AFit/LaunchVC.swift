//
//  LaunchVC.swift
//  AFit
//
//  Created by Dev1 on 3/15/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//
import UIKit
import AVFoundation

class LaunchVC: UIViewController, AVAudioPlayerDelegate {
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .Custom
    }
    
    @IBOutlet weak var videoWrapper: UIView!
    
    var playerLayer = AVPlayerLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let resourcePath = Bundle.main.path(forResource: "logovideo", ofType: "mp4")!
        let videoURL = URL(fileURLWithPath: resourcePath)
        let player = AVPlayer(url: videoURL)
        self.playerLayer = AVPlayerLayer(player: player)
        self.playerLayer.frame = self.view.bounds
        //AVLayerVideoGravityResizeAspect, AVLayerVideoGravityResizeAspectFill and AVLayerVideoGravityResize
        self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.videoWrapper.layer.addSublayer(playerLayer)
        player.play()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(LaunchVC.animationDidFinish(_:)),
                                               name: .AVPlayerItemDidPlayToEndTime,
                                               object: player.currentItem)
        
    }
    
    func animationDidFinish(_ notification: NSNotification) {
        
        let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewMainVC") as! NewMainVC
        let centerNav = UINavigationController(rootViewController: centerViewController)
        
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.drawerController!.centerViewController = centerNav
    }
    
}
