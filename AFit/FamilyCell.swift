//
//  FamilyCell.swift
//  AFit
//
//  Created by Dev1 on 12/22/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import SwiftyJSON

class FamilyCell: UITableViewCell {
    
    
    @IBOutlet weak var clubTitleLbl: UILabel!
    @IBOutlet weak var clubAddressLbl: UILabel!
    @IBOutlet weak var iconUnchecked: UIImageView!
    @IBOutlet weak var iconChecked: UIImageView!
    
    func configureCell(_ obj: JSON) {
        
        if obj != JSON.null{
            
            if let title = obj[0].rawString(), title != "" {
                self.clubTitleLbl.text = title.uppercased()
            } else {
                self.clubTitleLbl.isHidden = true
            }
            
            if let title = obj[2].rawString(), title != "" {
                self.clubAddressLbl.text = title.uppercased()
            } else {
                self.clubAddressLbl.isHidden = true
            }
            
            if Constants.URL_SLUG == obj[1].rawString() {
            
                self.iconUnchecked.alpha = 0.0
                self.iconChecked.alpha = 1.0
            
            } else {
            
                self.iconUnchecked.alpha = 1.0
                self.iconChecked.alpha = 0.0
                
            }
            
        }

        
    }
    
    
    
    
}
