//
//  About.swift
//  AFit
//
//  Created by Dev1 on 12/8/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import Foundation

class About {
    
    var _timeStamp: Int?
    
    var _title: String?
    
    var _featuredImg: Any?
    
    var _content: String?
    
    var timeStamp: Int? {
        return _timeStamp
    }
    
    var title: String? {
        return _title
    }
    
    var content: String? {
        return _content
    }
    
    var featuredImg: Any? {
        return _featuredImg
    }
    
    
    
    init (timeStamp: Int?, title: String?, content: String?, featuredImg: Any?) {
        self._title = title
        self._content = content
        self._featuredImg = featuredImg
        self._timeStamp = timeStamp
    }
}
