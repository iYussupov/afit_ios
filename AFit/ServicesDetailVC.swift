//
//  ServicesDetailVC.swift
//  AFit
//
//  Created by Dev1 on 3/10/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire
import AlamofireImage
import SwiftyJSON

class ServicesDetailVC: UIViewController, UIScrollViewDelegate {

    var services: Services!
    var toggleRightDrawer: Bool?
    @IBOutlet weak var featuredImg: UIImageView!
    
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var SendSignUpRequest: UIView!
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .Custom
        let value = UIInterfaceOrientation.portraitUpsideDown.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        scrollview.delegate = self
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(ServicesDetailVC.respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let signUptap = UITapGestureRecognizer(target: self, action: #selector(ServicesDetailVC.callServiceSignUpModal))
        
        self.SendSignUpRequest.addGestureRecognizer(signUptap)
        self.SendSignUpRequest.isUserInteractionEnabled = true
        
    }
    
    func callServiceSignUpModal() {
        
        let service_name = services.title
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ServiceSignupVC") as! ServiceSignupVC
        self.addChildViewController(popOverVC)
        popOverVC.service_name = service_name
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
        
    }
    
    func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                let _ = self.navigationController?.popViewController(animated: true)
            default:
                break
            }
        }
    }
    
    func updateUI() {
        
        self.contentLbl.text = services.content
        self.contentLbl.setLineHeight()
        
        self.titleLbl.text = services.title?.uppercased()
        
        self.headerTitle.text = services.title?.uppercased()
        
        let images = services.featuredImg
        
        let defaults = UserDefaults.standard
        
        var imgArray:[InputSource] = []
        
        for image in images as! JSON {
            
            let image_url = image.1["image_url"]
            
            if defaults.object(forKey:"\(image_url)") != nil {
                
                let cachedImg = UIImage(data: defaults.object(forKey:"\(image_url)") as! Data)
                
                imgArray.append(ImageSource(image: cachedImg! as UIImage))
                self.initSlideShow(imgArray: imgArray)
                
            } else {
                
                Alamofire.request("\(image_url)").responseImage { response in
                    
                    if let img = response.result.value {
                        
                        let imageData = UIImageJPEGRepresentation(img, 1.0)
                        
                        defaults.setValue(imageData, forKey: "\(image_url)")
                        
                        defaults.synchronize()
                        
                        imgArray.append(ImageSource(image: img))
                        self.initSlideShow(imgArray: imgArray)
                        
                    }
                    
                }
            }
            
        }
 
    }
    
    func initSlideShow(imgArray:[InputSource]) {
        
        slideshow.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.0);
        slideshow.slideshowInterval = 5.0
        slideshow.pageControlPosition = PageControlPosition.insideScrollView
        slideshow.pageControl.currentPageIndicatorTintColor = Constants.ThemeAccentColor
        slideshow.pageControl.pageIndicatorTintColor = UIColor.lightGray
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        
        slideshow.setImageInputs(imgArray)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(ServicesDetailVC.click))
        slideshow.addGestureRecognizer(recognizer)
        
    }
    
    @IBAction func DetailsBackBtn(_ sender: AnyObject) {
        
        if toggleRightDrawer != true {
            let _ = self.navigationController?.popViewController(animated: true)
        } else {
            self.evo_drawerController?.toggleRightDrawerSideAnimated(true, completion: nil)
        }
    }
    
    @IBAction func openProfile(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func click() {
        let ctr = FullScreenSlideshowViewController()
        ctr.pageSelected = {(page: Int) in
            self.slideshow.setScrollViewPage(page, animated: false)
        }
        
        ctr.initialImageIndex = slideshow.scrollViewPage
        ctr.inputs = slideshow.images
        ctr.slideshow.pageControl.currentPageIndicatorTintColor = Constants.ThemeAccentColor
        self.slideshowTransitioningDelegate = ZoomAnimatedTransitioningDelegate(slideshowView: slideshow, slideshowController: ctr)
        ctr.transitioningDelegate = self.slideshowTransitioningDelegate
        self.present(ctr, animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        var headerTransform = CATransform3DIdentity
        
        // PULL DOWN -----------------
        let headerScaleFactor:CGFloat = -(offset) / slideshow.bounds.height
        
        if offset < 0 {
            
            
            let headerSizevariation = ((slideshow.bounds.height * (1.0 + headerScaleFactor)) - slideshow.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation + offset, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            
            
            slideshow.layer.transform = headerTransform
        }
            
            // SCROLL UP/DOWN ------------
            
        else {
            
            // Header -----------
            
            headerTransform = CATransform3DTranslate(headerTransform, 0, 0, 0)
            
        }
        
        // Apply Transformations
        
        slideshow.layer.transform = headerTransform
    }
    
}
