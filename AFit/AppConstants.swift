//
//  AppConstants.swift
//  AFit
//
//  Created by Dev1 on 1/17/17.
//  Copyright © 2017 fxofficeapp. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct Constants {
    
    static let appName    = "A-Fit"
    
    static let APP_FAMILY = "A-Fit"
    
    static var URL_SLUG   = "/afit/"
    
    static var APP_NAME   = "A-Fit"
    
    static let webSiteUrl = "http://fitness.beapp.net"
    
    static let VC_id = "5762431"
    
    static let Kontakt_API = "AWGJEvBOafvAAkVuIxgwMPbyztJClrje"
    
    static let Google_API = "AIzaSyBeQqZTTe1lSOohpcV43nVRwGI8pc4ZnDA"
    
    static let ThemeMainColor   = UIColor(red: (118/255.0), green: (190/255.0), blue: (52/255.0), alpha: 1.0)
    static let ThemeAccentColor = UIColor(red: (252/255.0), green: (212/255.0), blue: (0/255.0), alpha: 1.0)
    static let ThemeGreenColor  = UIColor(red: (118/255.0), green: (190/255.0), blue: (52/255.0), alpha: 1.0)
    static let ThemeYellowColor = UIColor(red: (252/255.0), green: (212/255.0), blue: (0/255.0), alpha: 1.0)
    
    static let ThemeMenuStartColor = UIColor(red: (83/255.0), green: (139/255.0), blue: (32/255.0), alpha: 1.0)
    static let ThemeEndStartColor = UIColor(red: (8/255.0), green: (37/255.0), blue: (91/255.0), alpha: 0.60)
    
    static let URL_BASE       = "http://cp.beapp.net"
    
    static let URL_POSTS      = "wp-json/wp/v2/posts"
    static let URL_SCHEDULE   = "wp-json/wp/v2/schedule"
    static let URL_CATEGORIES = "wp-json/wp/v2/categories"
    static let URL_MAILER     = "wp-json/wp/v2/mail"
    static let URL_ABOUT      = "wp-json/wp/v2/aboutus"
    static let URL_PHOTO      = "wp-json/wp/v2/phototour"
    static let URL_TRAINERS   = "wp-json/wp/v2/trainers"
    static let URL_SERVICES   = "wp-json/wp/v2/services"
    static let URL_CONTACTS   = "wp-json/wp/v2/contacts"
    static let URL_CLASSES    = "wp-json/wp/v2/classes"
    static let URL_PUSH       = "push/get_archive/?orderby=date&order=desc"
    
    static let URL_FAMILY     = "/wp-json/wp/v2/family"
    
    static var APP_FAMILY_ARRAY: JSON = [:]
    
    static var DEVICE_TOKEN = ""
    
    static var TRAINING_FILTER_NAME = "ALL".localized
    static var DEFAULT_FILTER_NAME  = "ALL".localized
    
}

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
extension String {
    func convertHtmlSymbols() throws -> String? {
        guard let data = data(using: .utf8) else { return nil }
        
        return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil).string
    }
}
