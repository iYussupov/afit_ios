//
//  PopRoundedCorners.swift
//  AFit
//
//  Created by Dev1 on 11/30/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit

class PopRoundedCorners: UIView {
    
    override func awakeFromNib() {
        layer.cornerRadius = 4.0
    }
    
}
