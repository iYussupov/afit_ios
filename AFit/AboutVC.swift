//
//  AboutVC.swift
//  AFit
//
//  Created by Dev1 on 3/11/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire
import AlamofireImage
import SwiftyJSON

class AboutVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var contentField: UILabel!
    @IBOutlet weak var titleField: UILabel!
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet weak var viewMapBtn: UIView!
    @IBOutlet weak var mapViewContainer: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate?
    static var imageCache = NSCache<AnyObject, AnyObject>()
    var mapCollapsed = false
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .All
        let value = UIInterfaceOrientation.portraitUpsideDown.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        
        parseData()
        checkTimeStamp()
        
//        let tap = UITapGestureRecognizer(target: self, action:  #selector(AboutVC.animateMapViewHeight))
//        self.viewMapBtn.addGestureRecognizer(tap)
        
    }
    
    func checkTimeStamp() {
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"about_data_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"about_data_\(Constants.URL_SLUG)") as! String)
            
            let timeStampCached = object["time"].int
            
            
            
            Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_ABOUT)/").responseJSON { response in
                
                if let data = response.result.value {
                    
                    let object = JSON(data)
                    
                    let timeStamp = object["time"].int
                    
                    if timeStampCached != timeStamp {
                        
                        self.parseFreshData()
                        
                        print("refresh data")
                        
                    }
                    
                } else {
                
                    print("no internet")
                    
                }
                
            }
            
            print("checking timestamp")
            
        }
    
    }
    
    func parseData() {
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"about_data_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"about_data_\(Constants.URL_SLUG)") as! String)
        
            self.updateModel(object:object)
            
            print("cachedData a-ha")
            
        } else {
            
            parseFreshData()
            
        }
    }
    
    func parseFreshData() {
        
        let defaults = UserDefaults.standard
        
        Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_ABOUT)/").responseJSON { response in
            
            if let data = response.result.value {
                
                let object = JSON(data)
                
                defaults.setValue(object.rawString()!, forKey: "about_data_\(Constants.URL_SLUG)")
                
                defaults.synchronize()
                
                self.updateModel(object:object)
                
            }
            
        }
    }
    
    func updateModel(object:JSON){
        
        if object != false {
            
            let timeStamp = object["time"].int
            
            let title = object["club_title"].string
            
            let content = object["club_description"].string
            
            let featuredImg = object["club_image"]
            
            let about_data = About(timeStamp:timeStamp,title:title, content:content, featuredImg:featuredImg)
            
            self.updateUI(about_data)
        
        }
    }
    
    func updateUI(_ about: About) {

        
        self.contentField.alpha = 0.0
        let contentStartY = self.contentField.frame.origin.y + 25
        self.contentField.frame.origin.y = contentStartY
        self.contentField.text = about.content
        self.contentField.setLineHeight()
        
        
        self.titleField.alpha = 0.0
        let titleStartY = self.titleField.frame.origin.y + 25
        self.titleField.frame.origin.y = titleStartY
        self.titleField.text = about.title?.uppercased()
        
        let defaults = UserDefaults.standard
            
        let images = about.featuredImg
        
        var imgArray:[InputSource] = []
            
        for image in images as! JSON {
            
            if defaults.object(forKey:"\(image.1)") != nil {
                
                let cachedImg = UIImage(data: defaults.object(forKey:"\(image.1)") as! Data)
        
                imgArray.append(ImageSource(image: cachedImg! as UIImage))
                self.initSlideShow(imgArray: imgArray)
            
            } else {
                
                Alamofire.request("\(image.1)").responseImage { response in
                    
                    if let img = response.result.value {
                        
                        let imageData = UIImageJPEGRepresentation(img, 1.0)
                        
                        defaults.setValue(imageData, forKey: "\(image.1)")
                        
                        defaults.synchronize()
                        
                        imgArray.append(ImageSource(image: img))
                        self.initSlideShow(imgArray: imgArray)
                        
                    }
                    
                }
            }
            
        }
        
        UIView.animate(withDuration: 0.35, delay: 0.3, options: [.curveEaseOut], animations: {
            
            self.titleField.alpha = 1.0
            self.titleField.frame.origin.y = titleStartY - 25
            
        }, completion: nil)
        
        UIView.animate(withDuration: 0.35, delay: 0.5, options: [.curveEaseOut], animations: {
            
            self.contentField.alpha = 1.0
            self.contentField.frame.origin.y = contentStartY - 25
            
        }, completion: nil)
        
    }
    
    func initSlideShow(imgArray:[InputSource]) {
    
        slideshow.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.0);
        slideshow.slideshowInterval = 5.0
        slideshow.pageControlPosition = PageControlPosition.insideScrollView
        slideshow.pageControl.currentPageIndicatorTintColor = Constants.ThemeAccentColor
        slideshow.pageControl.pageIndicatorTintColor = UIColor.lightGray
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        
        slideshow.setImageInputs(imgArray)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(AboutVC.click))
        slideshow.addGestureRecognizer(recognizer)
        
    }

    @IBAction func openMenu(_ sender: AnyObject) {
        self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
        
    }
    
    @IBAction func openProfile(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func click() {
        let ctr = FullScreenSlideshowViewController()
        ctr.pageSelected = {(page: Int) in
            self.slideshow.setScrollViewPage(page, animated: false)
        }
        
        
        ctr.initialImageIndex = slideshow.scrollViewPage
        ctr.inputs = slideshow.images
        ctr.slideshow.pageControl.currentPageIndicatorTintColor = Constants.ThemeAccentColor
        self.slideshowTransitioningDelegate = ZoomAnimatedTransitioningDelegate(slideshowView: slideshow, slideshowController: ctr)
        ctr.transitioningDelegate = self.slideshowTransitioningDelegate
        self.present(ctr, animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        var headerTransform = CATransform3DIdentity
        
        // PULL DOWN -----------------
        let headerScaleFactor:CGFloat = -(offset) / slideshow.bounds.height
        
        if offset < 0 {
            
            
            let headerSizevariation = ((slideshow.bounds.height * (1.0 + headerScaleFactor)) - slideshow.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation + offset, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            
            
            slideshow.layer.transform = headerTransform
        }
            
            // SCROLL UP/DOWN ------------
            
        else {
            
            // Header -----------
            
            headerTransform = CATransform3DTranslate(headerTransform, 0, 0, 0)
            
        }
        
        // Apply Transformations
        
        slideshow.layer.transform = headerTransform
    }

}
