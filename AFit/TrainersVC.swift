//
//  TrainersVC.swift
//  AFit
//
//  Created by Dev1 on 3/10/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TrainersVC:  UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collection: UICollectionView!
    static var imageCache = NSCache<AnyObject, AnyObject>()
 
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .All
    }
    
    var trainers = [Trainers]()
    var lastPosition: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.parseData()
        self.checkTimeStamp()


    }
    
    func checkTimeStamp() {
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"trainers_data_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"trainers_data_\(Constants.URL_SLUG)") as! String)
            
            let timeStampCached = object["time"].int
            
            Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_TRAINERS)/").responseJSON { response in
                
                if let data = response.result.value {
                    
                    let object = JSON(data)
                    
                    let timeStamp = object["time"].int
                    
                    if timeStampCached != timeStamp {
                        
                        self.parseFreshData()
                        
                        print("refresh data")
                        
                    }
                    
                } else {
                    
                    print("no internet")
                    
                }
                
            }
            
            print("checking timestamp")
            
        }
        
    }
    
    func parseData(){
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"trainers_data_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"trainers_data_\(Constants.URL_SLUG)") as! String)
            
            self.updateModel(objects:object)
            
            print("cachedData a-ha")
            
        } else {
            
            self.parseFreshData()
            
        }
        
    }

    func parseFreshData() {
        
        self.trainers.removeAll()
        
        let defaults = UserDefaults.standard
        
        Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_TRAINERS)").responseJSON { response in
            
            if let data = response.result.value {
                
                let objects = JSON(data)
                
                defaults.setValue(objects.rawString()!, forKey: "trainers_data_\(Constants.URL_SLUG)")
                
                defaults.synchronize()
                
                self.updateModel(objects: objects)
                
                print("UPDATED")
            }
        }
        
    }
    
    func updateModel(objects:JSON){
        
        let timeStamp = objects["time"].int
        
        let trainers_objs = objects["trainers"]
        
        for (_,obj):(String, JSON) in trainers_objs {
            
            var name = ""
            
            do {
                name = try obj["name"].string!.convertHtmlSymbols()!
            } catch {
                
            }
            
            var content = ""
            
            do {
                content = try obj["content"].string!.convertHtmlSymbols()!
            } catch {
                
            }
            
            let featuredImg = obj["image_url"].string
            
            let category = obj["ocupation"].string
            
            let trainer = Trainers(timeStamp: timeStamp, name: name, featuredImg: featuredImg, category: category, content: content)
            
            self.trainers.append(trainer)
            
            self.collection.reloadData()
            
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrainersCell", for: indexPath) as? TrainersCell {
            
            let trainer: Trainers!
            trainer = trainers[(indexPath as NSIndexPath).row]
            
            cell.configureCell(trainer)
            
            let position = (indexPath as NSIndexPath).row
            
            if (position > lastPosition) {
                
                let startCellY = cell.frame.origin.y + 100
                cell.frame.origin.y = startCellY
                
                cell.alpha = 0.0
                
                UIView.animate(withDuration: 0.25, delay: Double(position) * 0.125, options: [.curveEaseOut], animations: {
                    
                    cell.alpha = 1.0
                    cell.frame.origin.y = startCellY - 100
                    
                }, completion: nil)
                
                lastPosition = position
                
            }
            
            return cell
            
        } else {
            
            return UICollectionViewCell()
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if trainers.count > 0 {
            let trainer = self.trainers[indexPath.row]
            performSegue(withIdentifier: "TrainerDetailVC", sender: trainer)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trainers.count
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        let screenSize: CGRect = UIScreen.main.bounds

        var viewWidth:CGFloat = 0.0
        var viewHeight:CGFloat = 0.0

        viewWidth = (screenSize.width - 42)/2
        
        
        let bigFont: UIFont = UIFont(name: "DINPro", size: 14)!
        let smallFont: UIFont = UIFont(name: "DINPro", size: 12)!
        
        let trainer: Trainers!
        trainer = trainers[(indexPath as NSIndexPath).row]
        
        let nameHeight = trainer.name?.heightWithConstrainedWidth(width: viewWidth, font: bigFont)
        let categoryHeight = trainer.category?.heightWithConstrainedWidth(width: viewWidth, font: smallFont)
        
        
        viewHeight = viewWidth + nameHeight! + categoryHeight! + 3
        
        return CGSize(width: viewWidth, height: viewHeight)
        
        
    }
    
    
    @IBAction func openMenu(_ sender: AnyObject) {
        self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
    }
    
    @IBAction func openProfile(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        self.navigationController?.pushViewController(controller, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TrainerDetailVC" {
            if let trainerDetailVC = segue.destination as? TrainerDetailVC {
                if let trainer = sender as? Trainers {
                    trainerDetailVC.trainer = trainer
                }
            }
        }
    }
    
    
    
}

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
//        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
//        
//        return boundingBox.height
        
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:width, height:CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = self
        
        label.sizeToFit()
        return label.frame.height
    }
}
