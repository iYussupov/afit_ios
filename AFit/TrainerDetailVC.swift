//
//  TrainerDetailVC.swift
//  AFit
//
//  Created by Dev1 on 3/10/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class TrainerDetailVC: UIViewController, UIScrollViewDelegate {
        
    var trainer: Trainers!
    var toggleRightDrawer: Bool?
    var preventAnimation = Set<IndexPath>()
        
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var contentField: UILabel!
        
    @IBOutlet weak var imageViewer: UIView!
        
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var backBtnView: UIButton!
        
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var SendSignUpRequest: UIView!
        
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .Custom
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        self.updateUI()
            
        let tap = UITapGestureRecognizer(target: self, action: #selector(TrainerDetailVC.showImageViewer))
        imageViewer.addGestureRecognizer(tap)
            
        let signUptap = UITapGestureRecognizer(target: self, action: #selector(TrainerDetailVC.callTrainerSignUpModal))
        
        self.SendSignUpRequest.addGestureRecognizer(signUptap)
        self.SendSignUpRequest.isUserInteractionEnabled = true
            
    }
    
    func callTrainerSignUpModal() {
        
        let trainer_name = trainer.name
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TrainerSignupVC") as! TrainerSignupVC
        self.addChildViewController(popOverVC)
        popOverVC.trainer_name = trainer_name
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
        
    }
    
    func showImageViewer() {
        let img = trainer.featuredImg
        performSegue(withIdentifier: "ViewerVC", sender: img)
    }
        
        
        
    func updateUI() {
            
        titleLbl.text = trainer.name?.uppercased()
        headerTitle.text = trainer.name?.uppercased()
            
        self.contentField.text = trainer.content
        self.contentField.setLineHeight()
            
        if let url = trainer.featuredImg {
            
            self.headerImage.af_setImage(withURL: NSURL(string: url) as! URL, placeholderImage: UIImage(named: "mask")!)
            
        } else {
            
            self.headerImage.image = UIImage(named: "mask")
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
            
        self.headerImage.clipsToBounds = true
            
    }
        
        
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            
        let offset = scrollView.contentOffset.y
        var headerTransform = CATransform3DIdentity
            
        // PULL DOWN -----------------
            
        if offset < 0 {
                
            let headerScaleFactor:CGFloat = -(offset) / headerImage.bounds.height
            let headerSizevariation = ((headerImage.bounds.height * (1.0 + headerScaleFactor)) - headerImage.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
                
            headerImage.layer.transform = headerTransform
        }
                
            // SCROLL UP/DOWN ------------
                
        else {
                
            // Header -----------
                
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
                
        }
            
        // Apply Transformations
            
        headerImage.layer.transform = headerTransform
    }
        
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    @IBAction func DetailsBackBtn(_ sender: AnyObject) {
            
        if toggleRightDrawer != true {
            let _ = self.navigationController?.popViewController(animated: true)
        } else {
            self.evo_drawerController?.toggleRightDrawerSideAnimated(true, completion: nil)
        }
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ViewerVC" {
            if let viewerVC = segue.destination as? ViewerVC {
                if let img = trainer.featuredImg {
                    viewerVC.img = img
                }
            }
        }
    }
        
    @IBAction func openProfile(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
        
        
}


