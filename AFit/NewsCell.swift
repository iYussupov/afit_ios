//
//  NewsCell.swift
//  AFit
//
//  Created by Ingwar on 3/5/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

class NewsCell: UITableViewCell {
        
        @IBOutlet weak var featuredImg: UIImageView!
    
        @IBOutlet weak var titleLbl: UILabel!
        
        @IBOutlet weak var categoryLbl: UILabel!
        
        @IBOutlet weak var dateLbl: UILabel!
        
        @IBOutlet weak var newsShareBtn: RoundBtnViewStyle!
        
        fileprivate var _news: News?
    
        var news: News? {
            return _news
        }
    
        override func draw(_ rect: CGRect) {
            featuredImg.clipsToBounds = true
        }
    
        
        func configureCell(_ news: News) {
            
            self._news = news
            
            self.featuredImg.af_cancelImageRequest()
            
            if let title = news.title , title != "" {
                self.titleLbl.text = title.uppercased()
            } else {
                self.titleLbl.text = nil
            }
            
            if let category = news.category , category != "" {
                self.categoryLbl.text = "\(category.uppercased())"
//                if category == "Акции" {
                    self.categoryLbl.backgroundColor = UIColor(red: 118.0/255.0, green: 190.0/255.0, blue: 52.0/255.0, alpha: 1.0)
                    self.categoryLbl.layer.borderColor = UIColor(red: (118.0/255.0), green: (190.0/255.0), blue: (52.0/255.0), alpha: 1.0).cgColor
//                } 
            } else {
                self.categoryLbl.isHidden = true
            }
            
            
            if let date = news.date {
                
                let Date:DateFormatter = DateFormatter()
                Date.dateFormat = "dd.MM.yyyy"
                
                self.dateLbl.text = Date.string(from: date)
                
            }
            
            
            if let url = news.featuredImg {
            
                self.featuredImg.af_setImage(withURL: NSURL(string: url) as! URL, placeholderImage: UIImage(named: "mask")!)
                
            } else {
                
                self.featuredImg.image = UIImage(named: "mask")
                
            }
            
            self.newsShareBtn.addTarget(self, action: #selector(NewsCell.newsShare(sender:)), for: .touchUpInside)
            
        }
    
        func newsShare(sender: UIButton){
//            let buttonTag = sender.tag
            var textToShare = ""
            
            if news!.title != nil {
                textToShare = "\(news!.title!)"
            }
            
            
            if let myWebsite = NSURL(string: "http://")
            {
                let objectsToShare = [textToShare, myWebsite] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                //New Excluded Activities Code
                activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
                //
                
                // self.presentViewController(activityVC, animated: true, completion: nil)
                UIApplication.shared.keyWindow?.rootViewController?.present(activityVC, animated: true, completion: nil)
            }
        }

    
    
    
}
