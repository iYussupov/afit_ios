//
//  PushVC.swift
//  AFit
//
//  Created by Dev1 on 12/19/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PushVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .All
    }
    
    var pushes = [Push]()
    var preventAnimation = Set<IndexPath>()
    var viewInitialized: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView.init(frame: CGRect(x:0, y:0, width:self.tableView.frame.size.width, height:1))

        parseFreshData()
    }
    
    func parseFreshData() {
        
        Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_PUSH)/").responseJSON { response in
            
            if let data = response.result.value {
                
                let objects = JSON(data)
                
                for (_,object):(String, JSON) in objects["result"] {
                    
                    let message = object["message"].string
                    
                    let date = object["starttime"].string
                    
                    let dateFormatter = DateFormatter()
                    
                    let createdDate = dateFormatter.dateFromCustomString(dateString:date!)
                    
                    let post_id = object["post_id"].int
                    
                    let push = Push(message: message, date: createdDate, post_id: post_id)
                    
                    self.pushes.append(push)
                    
                    self.tableView.reloadData()
                    
                }
                
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pushes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let push = self.pushes[(indexPath as NSIndexPath).row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PushCell") as? PushCell {
            
            let tap = UITapGestureRecognizer(target: self, action:#selector(pushAction(withSender:)))
            
            cell.pushActionBtn.tag = (indexPath as NSIndexPath).row
            
            cell.pushActionBtn.addGestureRecognizer(tap)
            cell.pushActionBtn.isUserInteractionEnabled = true
            
            if (indexPath as NSIndexPath).row == self.pushes.count - 1 {
                
                cell.divider.isHidden = true
            
            } else {
                
                cell.divider.isHidden = false
            
            }
            
            cell.configureCell(push)
            
            return cell
        } else {
            return PushCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let position = (indexPath as NSIndexPath).row
        
        if !self.viewInitialized  {
            
            let startCellY = cell.frame.origin.y + 50
            cell.frame.origin.y = startCellY
            
            cell.alpha = 0.0
            
            UIView.animate(withDuration: 0.25, delay: Double(position + 1) * 0.125, options: [.curveEaseOut], animations: {
                
                cell.alpha = 1.0
                cell.frame.origin.y = startCellY - 50
                
            }, completion: nil)
            
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.viewInitialized = true
        }
        
    }
    
    func pushAction(withSender sender: AnyObject) {
            
        let index = sender.view?.tag
        
        if let postID = pushes[index!].post_id {
            
            
        
        Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_POSTS)/\(postID)").responseJSON { response in
            
            
            if let data = response.result.value {
                
                let object = JSON(data)
                
                let id = object["id"].int
                
                let timeStamp = object["timestamp"].int
                
                var title = ""
                
                do {
                    title = try object["title"]["rendered"].string!.convertHtmlSymbols()!
                } catch {
                    
                }
                
                var content = ""
                
                do {
                    content = try object["content"]["rendered"].string!.convertHtmlSymbols()!
                } catch {
                    
                }
                
                let featuredImg = object["featured_image_thumbnail_url"].string
                
                let date = object["date"].string
                
                let dateFormatter = DateFormatter()
                
                let createdDate = dateFormatter.dateFromSwapiString(dateString:date!)
                
                let category = object["post_category_name"].string
                
                let news = News(timeStamp:timeStamp, postId:id,title:title, content:content, featuredImg:featuredImg, category:category, date: createdDate)
                
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                let centerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewsDetailVC") as! NewsDetailVC
                centerViewController.news = news
                centerViewController.fromPushDirectly = false
                centerViewController.toggleRightDrawer = true
                
                self.navigationController?.pushViewController(centerViewController, animated: true)
                
            }
            
        }
            
        }
        
        
    }
    
    @IBAction func openMenu(_ sender: AnyObject) {
        self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
        
    }
    
    @IBAction func openProfile(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        self.navigationController?.pushViewController(controller, animated: true)
    }


}
