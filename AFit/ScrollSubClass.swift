//
//  ScrollSubClass.swift
//  AFit
//
//  Created by Dev1 on 3/18/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit

class ScrollSubClass: UIScrollView {

    override func touchesShouldCancel(in view: (UIView!)) -> Bool {
        if view is UIButton {
            return  true
        }
        return  super.touchesShouldCancel(in: view)
    }

}
