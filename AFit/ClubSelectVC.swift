//
//  ClubSelectVC.swift
//  AFit
//
//  Created by Dev1 on 12/22/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import SwiftyJSON

class ClubSelectVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var family: JSON = Constants.APP_FAMILY_ARRAY
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var modalWrapper: UIView!
    @IBOutlet weak var modalContainerHeight: NSLayoutConstraint!
    
    @IBAction func dismissPopUp(_ sender: Any) {
            
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
            
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.modalWrapper.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        UIView.animate(withDuration: 0.5, animations: {
            self.modalWrapper.alpha = 1.0
        })
        
        self.refreshPopUpHeight()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return family.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let familyObj = self.family[(indexPath as NSIndexPath).row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyCell") as? FamilyCell {
            
            cell.configureCell(familyObj)
            
            return cell
        } else {
            
            return FamilyCell()
            
        }
        
    }
    
    func childFunction() {
        NewMainVC.instance?.clubDidSelected()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let familyObj = self.family[(indexPath as NSIndexPath).row]
        
        if Constants.APP_NAME != familyObj[0].rawString() {
        
            let cell = tableView.cellForRow(at: indexPath as IndexPath)
        
            cell?.selectionStyle = .none
        
            self.tokenUnregister()
        
            let defaults = UserDefaults.standard
        
            defaults.setValue(familyObj[0].rawString()!, forKey: "default_app_name")
        
            defaults.setValue(familyObj[1].rawString()!, forKey: "default_app_slug")
        
            defaults.synchronize()
        
            Constants.APP_NAME = familyObj[0].rawString()!
        
            Constants.URL_SLUG = familyObj[1].rawString()!
        
            tableView.reloadData()
        
            self.childFunction()
        
            self.tokenRegister()
            
            
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
            
            
        }
        
    }
    
    func refreshPopUpHeight(){
        
        var height:CGFloat = self.tableView.rowHeight
        height *= CGFloat(family.count)
        
        var tableFrame:CGRect = self.tableView.frame
        tableFrame.size.height = 120.0
        self.tableView.frame = tableFrame;
        
        
        let screenHeight = UIScreen.main.bounds.height
        let tableViewHeight = tableView.contentSize.height
        var newModalHeight = tableViewHeight - 50
        
        if newModalHeight > screenHeight - 32 {
            newModalHeight = screenHeight - 32
        }
        if newModalHeight < 300 {
            newModalHeight = 300
        }
        
        self.modalContainerHeight.constant = newModalHeight
    
    }
    
    func tokenUnregister() {
    
        let deviceTokenString = Constants.DEVICE_TOKEN
        
        // create the request
        let url = URL(string: "\(Constants.URL_BASE)\(Constants.URL_SLUG)push/deletetoken/?device_token=\(deviceTokenString)&device_type=ios")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let urlConnection = NSURLConnection(request: request, delegate: self)
        
    }
    
    func tokenRegister() {
        
        let deviceTokenString = Constants.DEVICE_TOKEN
        
        // create the request
        let url = URL(string: "\(Constants.URL_BASE)\(Constants.URL_SLUG)push/savetoken/?device_token=\(deviceTokenString)&device_type=ios&channels_id=1")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let urlConnection = NSURLConnection(request: request, delegate: self)
        
    }
    
}
