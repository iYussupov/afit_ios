//
//  CalendarVC.swift
//  AFit
//
//  Created by Dev1 on 3/12/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


var weekView: CalendarWeekViewControllerView?

class CalendarVC: UIViewController, UITableViewDataSource, UITableViewDelegate, CalendarWeekViewControllerDelegate {

    var selectedIndex = -1
    var results = [UILabel]()
    var scheduleList = [Schedule]()
    var scheduleTemp = [Schedule]()
    static var instance:CalendarVC?
    var cachedSelectedDay: GDate = GDate()
    var scheduleInitialized: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view, typically from a nib.
        weekView = CalendarWeekViewControllerView(frame: CGRect(x: 0, y: 130, width: view.frame.width, height: 44))
        view.addSubview(weekView!)
        
        
        
        let calendarMenu = CalendarMenuView(frame: CGRect(x: 0, y: 82, width: view.frame.width, height: 22))
        view.addSubview(calendarMenu)
        
        
        tableView.dataSource = self
        tableView.delegate = self
        weekView?.delegate = self
        
        var i = 0
        var currDayIndex = 0
        for current in (weekView?.presentWeek)! {
            if current == weekView?.selectedDay{
                currDayIndex = i
            }
            i += 1
        }
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(CalendarVC.respondToSwipeGesture(_:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.tableView.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(CalendarVC.respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.tableView.addGestureRecognizer(swipeRight)
        
        
        
        self.parseData(currDayIndex: currDayIndex)
        self.checkTimeStamp(currDayIndex: currDayIndex)
        
        self.parseClassesData();

    }
    
    
    
    func parseClassesData() {
        
        let defaults = UserDefaults.standard
        
        Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_CLASSES)/").responseJSON { response in
            
            if let data = response.result.value {
                
                let object = JSON(data)
                
                let timeStamp = object["time"].int
                
                defaults.setValue(timeStamp, forKey: "classes_time_created_\(Constants.URL_SLUG)")
                
                defaults.setValue(object.rawString()!, forKey: "classes_data_\(Constants.URL_SLUG)")
                
                defaults.synchronize()
                
            }
            
        }
    }
    
    override func awakeFromNib() {
        type(of: self).instance = self
    }
    
    func daySelect() {
    }
    
    func nextWeekView() {
        self.addScheduleToWeekday(currentDayIndex:0,  schedule_list:self.scheduleList, from:.left)
        
    }
    func prevWeekView(){
        self.addScheduleToWeekday(currentDayIndex:0,  schedule_list:self.scheduleList, from:.right)
        
    }
    func afterAutoScroll(){
    }
    
    
    
    func toNextDay() {}
    
    func toPrevDay() {}
    
    func tapOnDay() {
        
        self.parsingSelectedDay()
        
    }
    
    func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            if swipeGesture.direction == UISwipeGestureRecognizerDirection.left {
                weekView!.scrollToNextDay()
            }
            
            if swipeGesture.direction == UISwipeGestureRecognizerDirection.right {
                weekView!.scrollToPrevDay()
            }
            parsingSelectedDay()
        }
        
    }
        
        
        @IBOutlet weak var tableView: UITableView!
        
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.isNavigationBarHidden = true
            self.evo_drawerController?.openDrawerGestureModeMask = .BezelPanningCenterView
        }
        
        var schedule = [Schedule]()
        var preventAnimation = Set<IndexPath>()
    
    
    
    func parsingSelectedDay(){
        var i = 0
        var currentDayIndex = 0
        var from:UITableViewRowAnimation = .left
        for current in (weekView?.presentWeek)! {
            if current == weekView?.selectedDay{
                currentDayIndex = i
            }
            i += 1
        }
        
        if (weekView?.selectedDay.timeInterval)! < self.cachedSelectedDay.timeInterval {
            from = .right
        }
        
        self.cachedSelectedDay = (weekView?.selectedDay)!
        
        self.addScheduleToWeekday(currentDayIndex:currentDayIndex,  schedule_list:self.scheduleList, from:from)
    }
    
    func checkTimeStamp(currDayIndex:Int) {
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"schedule_data_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"schedule_data_\(Constants.URL_SLUG)") as! String)
            
            let timeStampCached = object["time"].int
            
            Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_SCHEDULE)/").responseJSON { response in
                
                if let data = response.result.value {
                    
                    let object = JSON(data)
                    
                    let timeStamp = object["time"].int
                    
                    if timeStampCached != nil, timeStampCached != timeStamp {
                        
                        self.scheduleList.removeAll()
                        
                        self.parseFreshData(currDayIndex: currDayIndex)
                        
                        print("refresh data")
                        
                    }
                    
                } else {
                    
                    print("no internet")
                    
                }
                
            }
            
            print("checking timestamp")
            
        }
        
    }
    
    func parseData(currDayIndex:Int){
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"schedule_data_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"schedule_data_\(Constants.URL_SLUG)") as! String)
            
            self.updateModel(objects:object,currDayIndex:currDayIndex)
            
            print("cachedData a-ha")
            
        } else {
            
            self.parseFreshData(currDayIndex:currDayIndex)
            
        }
        
    }
    
        
    func parseFreshData(currDayIndex:Int) {
        
        let defaults = UserDefaults.standard
        
        Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_SCHEDULE)/").responseJSON { response in
        
            if let data = response.result.value {
                
                let objects = JSON(data)
                
                defaults.setValue(objects.rawString()!, forKey: "schedule_data_\(Constants.URL_SLUG)")
                
                defaults.synchronize()
                
                self.updateModel(objects:objects,currDayIndex:currDayIndex)
                
                print("UPDATED")
                
            }
            
        }
        
    }
    
    func updateModel(objects:JSON,currDayIndex:Int){
        
        let timeStamp = objects["time"].int
        
        let schedule_obj = objects["schedule"]
        
        for (_,object):(String, JSON) in schedule_obj {
            
            for index in 0...object.count - 1 {
                
                var title = ""
                
                do {
                    title = try object[index]["class_title"].string!.convertHtmlSymbols()!
                } catch {
                    
                }
                
                var content = ""
                
                do {
                    content = try object[index]["class_desc"].string!.convertHtmlSymbols()!
                } catch {
                    
                }
                
                var place = ""
                if object[index]["location_title"].string != "-" {
                    do {
                        place = try object[index]["location_title"].string!.convertHtmlSymbols()!
                    } catch {
                        
                    }
                }
                
                let start_date = object[index]["start_date"].string
                
                let end_date = object[index]["end_date"].string
                
                let time = object[index]["start_time"].string
                
                let duration = object[index]["duration"].string
                
                let weekday = Int(object[index]["weekday"].string!)!
                
                let scheduleItem = Schedule(timeStamp:timeStamp, weekday: weekday, title: title, content: content, place: place, start_date: start_date, end_date: end_date, time: time, duration: duration)
                
                self.scheduleList.append(scheduleItem)
                
            }
            
        }
        
        self.addScheduleToWeekday(currentDayIndex: currDayIndex, schedule_list: self.scheduleList)
    }
    
    func addScheduleToWeekday(currentDayIndex:Int,  schedule_list:[Schedule], from:UITableViewRowAnimation = .left) {
        
        schedule.removeAll()
        
        var checkDay = currentDayIndex + 1
        
        if checkDay == 7 { checkDay = 0}
        
        for obj in schedule_list {
            
            let weekday = getDayOfWeek(obj.start_date!)! - 1
            
            let start_date = GDate.init(obj.start_date!, dateFormat: "dd-MM-yyyy")
            
            if start_date.timeInterval < (weekView?.selectedDay.timeInterval)! {
            
                if let obj_end_date = obj.end_date, obj_end_date != "" {
                
                    var end_date = GDate.init(obj_end_date, dateFormat: "dd-MM-yyyy")
            
                    if end_date.addDay(1).timeInterval > (weekView?.selectedDay.timeInterval)! {
                    
                        if weekday == checkDay {
                        
                            self.schedule.append(obj)
                        
                        }
                    
                    }
            
                } else {
                
                    if weekday == checkDay {
                    
                        self.schedule.append(obj)
                    
                    }
                
                }
                
            }
            
        }
    
        
        self.schedule.sort{ $0.time! < $1.time!}
        self.scheduleTemp = self.schedule
        self.scheduleDidFiltered(from:from)
        self.cachedSelectedDay = (weekView?.selectedDay)!
    }
    
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return schedule.count
        }
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let schedule = self.schedule[(indexPath as NSIndexPath).row]
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleCell") as? ScheduleCell {
                
                cell.configureCell(schedule)
                
                let tap = UITapGestureRecognizer(target: self, action:#selector(callSignUpModal(withSender:)))
                
                cell.scheduleActionBtn.tag = (indexPath as NSIndexPath).row
                
                cell.scheduleActionBtn.addGestureRecognizer(tap)
                cell.scheduleActionBtn.isUserInteractionEnabled = true
            
                return cell
                
            } else {
                
                return ScheduleCell()
            }
            
        }
    
        func callSignUpModal(withSender sender: AnyObject) {
            
            
            
            let currentD = weekView?.selectedDay
            let today = GDate().minusDay(1)
            
            if (currentD?.timeInterval)! > today.timeInterval {
        
                let index = sender.view?.tag
        
                let data = self.schedule[index!]
            
                let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TrainingSignupVC") as! TrainingSignupVC
                self.addChildViewController(popOverVC)
                popOverVC.schedule = data
                popOverVC.view.frame = self.view.frame
                self.view.addSubview(popOverVC.view)
                popOverVC.didMove(toParentViewController: self)
                
                
            } else {
                
                self.showDialog(title:"Oops!".localized, message: "It's too late to register for this class already!".localized)
                            
            }

        
        }
    
    func showDialog(title:String, message: String){
        
        
        let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
    }
    
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            
            
            if !scheduleInitialized {
                
                let position = (indexPath as NSIndexPath).row
                cell.alpha = 0.0
                let startCellY = cell.frame.origin.y + 42
                cell.frame.origin.y = startCellY
                
                
                UIView.animate(withDuration: 0.25, delay: Double(position + 1) * 0.075, options: [.curveEaseOut], animations: {
                    
                    cell.alpha = 1.0
                    cell.frame.origin.y = startCellY - 50
                    
                }, completion: nil)
                
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.scheduleInitialized = true
            }
            
        }
        
        func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return 58
        }

    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if selectedIndex == (indexPath as NSIndexPath).row {
                return UITableViewAutomaticDimension
            } else {
                return 58
            }
        }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if selectedIndex == (indexPath as NSIndexPath).row {
                selectedIndex = -1
                tableView.beginUpdates()
                tableView.endUpdates()
            } else
            
            if selectedIndex != -1 {
                selectedIndex = (indexPath as NSIndexPath).row
                tableView.beginUpdates()
                tableView.endUpdates()
            } else {
            
                selectedIndex = (indexPath as NSIndexPath).row
                tableView.beginUpdates()
                tableView.endUpdates()
            
            }
        }
    
    func scheduleDidFiltered(from:UITableViewRowAnimation = .left) {
        
        self.schedule.removeAll()
        
        if Constants.TRAINING_FILTER_NAME != Constants.DEFAULT_FILTER_NAME {
            
            for newSchedule in self.scheduleTemp where newSchedule.title == Constants.TRAINING_FILTER_NAME {
                
                self.schedule.append(newSchedule)
                
            }
        
        } else {
            
            self.schedule.removeAll()
            self.schedule = self.scheduleTemp
        
        }
        
        
        DispatchQueue.main.async {
            
            if self.scheduleInitialized {
                let range = NSMakeRange(0, self.tableView.numberOfSections)
                let sections = IndexSet(integersIn: range.toRange() ?? 0..<0)
                self.tableView.reloadSections(sections, with: from)
            } else {
                self.tableView.reloadData()
            }
            
        }
        
    }
        
    @IBAction func openMenu(_ sender: AnyObject) {
        self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
    }
        
    @IBAction func openProfile(_ sender: AnyObject) {
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TrainingsFilterVC") as! TrainingsFilterVC
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    }
    
    
}



