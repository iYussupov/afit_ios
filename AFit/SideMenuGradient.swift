//
//  SideMenuGradient.swift
//  AFit
//
//  Created by Dev1 on 12/14/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

class SideMenuGradient: UIView {
    
    override func draw(_ rect: CGRect) {
        
        // Setup view
        
        let color1 = Constants.ThemeMenuStartColor.cgColor as CGColor
        let color2 = UIColor(red: (5/255.0), green: (18/255.0), blue: (37/255.0), alpha: 1.0).cgColor as CGColor
        let colors = [color1,color2] as CFArray
        let locations = [ 0.0, 0.6 ] as [CGFloat]
        let radius = min((self.bounds.size.height + 200), (self.bounds.size.height + 200))
        let center = CGPoint.init(x: 0.0, y: 0.0)
        
        // Prepare a context and create a color space
        let context = UIGraphicsGetCurrentContext()
        context!.saveGState()
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        // Create gradient object from our color space, color components and locations
        let gradient = CGGradient.init(colorsSpace: colorSpace, colors: colors, locations: locations)
        
        // Draw a gradient
        context!.drawRadialGradient(gradient!, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: radius, options: CGGradientDrawingOptions(rawValue: 0))
        context?.restoreGState()
    }
}
