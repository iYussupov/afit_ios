//
//  TrainerSignupVC.swift
//  AFit
//
//  Created by Dev1 on 12/27/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import Alamofire

class TrainerSignupVC: UIViewController, UITextFieldDelegate {
        
    var trainer_name: String!
    
    @IBAction func dissmissPopUp(_ sender: Any) {
            
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
            
    }
        
    @IBOutlet weak var dateAndTimeSelector: UIDatePicker!
    @IBOutlet weak var SendSignUpRequest: UIView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var modalWrapper: UIView!
    @IBOutlet weak var trainerName: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
        
    
    override func viewWillAppear(_ animated: Bool) {
        self.modalWrapper.alpha = 0.0
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 10, right: 0)
            self.scrollView.contentInset = contentInsets;
            self.scrollView.scrollIndicatorInsets = contentInsets;
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom:0, right: 0)
        self.scrollView.contentInset = contentInsets;
        self.scrollView.scrollIndicatorInsets = contentInsets;
    }
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
            
            
        UIView.animate(withDuration: 0.5, animations: {
            self.modalWrapper.alpha = 1.0
        })
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
            
        self.trainerName.text = trainer_name.uppercased()
        
        self.dateAndTimeSelector.minimumDate = Date()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(sendMail(_:)))
            
        self.SendSignUpRequest.addGestureRecognizer(tap)
        self.SendSignUpRequest.isUserInteractionEnabled = true
            
        self.nameField.delegate = self
        self.phoneField.delegate = self
    }
        
        
        
    func sendMail(_ sender: UITapGestureRecognizer) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd:MM:yyyy"
        
        let training_date = dateFormatter.string(from: self.dateAndTimeSelector.date)
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        
        let training_time = timeFormatter.string(from: self.dateAndTimeSelector.date)
        
        let token = Constants.DEVICE_TOKEN
            
        if let client_name = self.nameField.text, !client_name.isEmpty, let client_phone = self.phoneField.text, !client_phone.isEmpty {
                
            let parameters: Parameters = ["action":"trainer", "token":token, "device":"ios", "client_phone": client_phone,"client_name": client_name, "trainer_name": self.trainerName.text!,
                                              "training_time": training_time,"training_date": training_date ]
                
            Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_MAILER)/", parameters: parameters).responseString { response in
                    
                if response.result.value != nil {
                        
                    self.showDialog(title:"Thank you!".localized, message: "Your application has been received and will be confirmed shortly.".localized)
                        
                        
                    self.removeFromParentViewController()
                    self.view.removeFromSuperview()
                        
                } else {
                        
                    print("signup error")
                
                }
                    
            }
                
        } else {
                
            showDialog(title:"Error!".localized, message: "Please fill out the required fields".localized)
                
        }
        
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
        
    func showDialog(title:String, message: String){
            
            
        let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                
        }))
            
        present(refreshAlert, animated: true, completion: nil)
            
    }
        
        
        
        
}

