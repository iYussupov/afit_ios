//
//  ServicesVC.swift
//  AFit
//
//  Created by Dev1 on 3/10/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ServicesVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
        
        
    @IBOutlet weak var tableView: UITableView!
        
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .All
    }
        
    var services = [Services]()
    var preventAnimation = Set<IndexPath>()
    var viewInitialized: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        tableView.dataSource = self
        tableView.delegate = self
            
        self.parseData()
        self.checkTimeStamp()
    }
    
    func checkTimeStamp() {
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"services_data_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"services_data_\(Constants.URL_SLUG)") as! String)
            
            let timeStampCached = object["time"].int
            
            
            Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_SERVICES)/").responseJSON { response in
                
                if let data = response.result.value {
                    
                    let object = JSON(data)
                    
                    let timeStamp = object["time"].int
                    
                    if timeStampCached != timeStamp {
                        
                        self.parseFreshData()
                        
                        print("refresh data")
                        
                    }
                    
                } else {
                    
                    print("no internet")
                    
                }
                
            }
            
            print("checking timestamp")
            
        }
        
    }
    
    func parseData() {
        
        let defaults = UserDefaults.standard
        
        if defaults.object(forKey:"services_data_\(Constants.URL_SLUG)") != nil {
            
            let object = JSON.parse(defaults.object(forKey:"services_data_\(Constants.URL_SLUG)") as! String)
            
            self.updateModel(objects:object)
            
            print("cachedData a-ha")
            
        } else {
            
            parseFreshData()
            
        }
    }
        
    func parseFreshData() {
        
        self.services.removeAll()
        
        let defaults = UserDefaults.standard
        
        Alamofire.request("\(Constants.URL_BASE)\(Constants.URL_SLUG)\(Constants.URL_SERVICES)").responseJSON { response in
            
            if let data = response.result.value {
                
                let objects = JSON(data)
                
                defaults.setValue(objects.rawString()!, forKey: "services_data_\(Constants.URL_SLUG)")
                
                defaults.synchronize()
                
                self.updateModel(objects: objects)
                
                print("UPDATED")
            }
        }
        
    }
    
    func updateModel(objects:JSON){
        
        let timeStamp = objects["time"].int
        
        let services_objs = objects["services"]
        
        for (_,obj):(String, JSON) in services_objs {
            
            let title = obj["service_name"].string
            
            let content = obj["service_description"].string
            
            let featuredImg = obj["service_images"]
            
            let service = Services(timeStamp: timeStamp, title: title, content: content, featuredImg: featuredImg, icon: "", thumb: "")
            
            self.services.append(service)
            
            self.tableView.reloadData()
            
        }
        
    }

    
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return services.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let services = self.services[(indexPath as NSIndexPath).row]
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell") as? ServicesCell {
                
                cell.configureCell(services)
                
                return cell
                
            } else {
                return ServicesCell()
            }
            
        }
        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            
            let position = (indexPath as NSIndexPath).row
            
            if !self.viewInitialized  {
                
                let startCellY = cell.frame.origin.y + 50
                cell.frame.origin.y = startCellY
                
                cell.alpha = 0.0
                
                UIView.animate(withDuration: 0.25, delay: Double(position + 1) * 0.075, options: [.curveEaseOut], animations: {
                    
                    cell.alpha = 1.0
                    cell.frame.origin.y = startCellY - 50
                    
                }, completion: nil)
                
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.viewInitialized = true
            }
            
        }
    
        func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            let bounds = UIScreen.main.bounds
            var height = (bounds.size.height - 86) / CGFloat(services.count)
            if height > 100 { height = 100 }
            if height < 80 { height = 80 }
            return height
        }
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            let bounds = UIScreen.main.bounds
            var height = (bounds.size.height - 86) / CGFloat(services.count)
            if height > 100 { height = 100 }
            if height < 80 { height = 80 }
            return height
        }

        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            let news = self.services[(indexPath as NSIndexPath).row]
            performSegue(withIdentifier: "ServicesDetailVC", sender: news)
            
        }
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "ServicesDetailVC" {
                if let servicesDetailVC = segue.destination as? ServicesDetailVC {
                    if let services = sender as? Services {
                        servicesDetailVC.services = services
                    }
                }
            }
        }
        
        @IBAction func openMenu(_ sender: AnyObject) {
            self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
            
        }
        
    @IBAction func openProfile(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        self.navigationController?.pushViewController(controller, animated: true)
        }
        
        
}


