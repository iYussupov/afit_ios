//
//  ProfileVC.swift
//  AFit
//
//  Created by Dev1 on 3/9/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .All
    }

    @IBOutlet weak var addTwitterWrapper: PanelsViewBorder!
    @IBOutlet weak var addFacebookWrapper: PanelsViewBorder!
    @IBOutlet weak var addFacebook: UIStackView!
    @IBOutlet weak var addTwitter: UIStackView!
    @IBOutlet weak var facebookBonusLbl: UILabel!
    @IBOutlet weak var twitterBonusLbl: UILabel!
    @IBOutlet weak var totalBonusesLbl: UILabel!
    @IBOutlet weak var facebookAddingLbl: UILabel!
    @IBOutlet weak var twitterAddingLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let facebookTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.addFacebookFunc(_:)))
        self.addFacebookWrapper.addGestureRecognizer(facebookTap)
        
        let twitterTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.addTwitterFunc(_:)))
        self.addTwitterWrapper.addGestureRecognizer(twitterTap)
        
    }
    
    func addFacebookFunc(_ tap:UITapGestureRecognizer) {
        
        let currentBonus = Int(self.totalBonusesLbl.text!)! + 10
        
        self.totalBonusesLbl.text = "\(currentBonus)"
        
        self.addFacebook.alpha = 1.0
        
        self.facebookBonusLbl.isHidden = true
        
        self.facebookAddingLbl.text = "CONNECTED".localized
        
        self.addFacebookWrapper.removeGestureRecognizer(tap)
    }
    
    func addTwitterFunc(_ tap:UITapGestureRecognizer) {
        
        let currentBonus = Int(self.totalBonusesLbl.text!)! + 10
        
        self.totalBonusesLbl.text = "\(currentBonus)"
        
        self.addTwitter.alpha = 1.0
        
        self.twitterBonusLbl.isHidden = true
        
        self.twitterAddingLbl.text = "CONNECTED".localized
        
        self.addTwitterWrapper.removeGestureRecognizer(tap)
    }

    @IBAction func openMenu(_ sender: AnyObject) {
        self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
        
    }
    
}
