//
//  News.swift
//  AFit
//
//  Created by Ingwar on 3/5/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import Foundation

class News {
    
    var _timeStamp: Int?
    
    fileprivate var _postId: Int?
    
    fileprivate var _title: String?
    
    fileprivate var _featuredImg: String?
    
    fileprivate var _content: String?
    
    fileprivate var _category: String?
    
    fileprivate var _date: Date?
    
    fileprivate var _commentCount: Int!
    
    fileprivate var _postKey: String!
    
    fileprivate var _postUrl:String!
    
    var timeStamp: Int? {
        return _timeStamp
    }
    
    var postId: Int? {
        return _postId
    }
    
    var title: String? {
        return _title
    }
    
    var content: String? {
        return _content
    }
    
    var featuredImg: String? {
        return _featuredImg
    }
    
    var category: String? {
        return _category
    }
    
    var date: Date? {
        return _date
    }
    
    var commentCount: Int? {
        return _commentCount
    }
    
    var postKey: String? {
        return _postKey
    }
    

    
    init (timeStamp:Int?,postId:Int?, title: String?, content: String?, featuredImg: String?, category: String?, date: Date?) {
        self._timeStamp = timeStamp
        self._title = title
        self._content = content
        self._category = category
        self._featuredImg = featuredImg
        self._date = date
        self._postId = postId
    }
    
//    init(dictionary: AnyObject) {
//        
//        if let postId = dictionary["id"] as? Int {
//            self._postId = postId
//        }
//        
//        if let title = dictionary["ping_status"] as? String {
//            self._title = title
//        }
//        
//        if let content = dictionary["content"] as? String {
//            self._content = content
//        }
//        
//        if let category = dictionary["category"] as? String {
//            self._category = category
//        }
//        
//        if let featuredImg = dictionary["featuredImage"] as? String {
//            self._featuredImg = featuredImg
//        }
//        
//    }
    
}
