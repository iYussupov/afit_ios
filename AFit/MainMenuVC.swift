//
//  MainMenuVC.swift
//  AFit
//
//  Created by Dev1 on 3/9/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import UIKit

class MainMenuVC: UIViewController {
    
    @IBOutlet weak var scrllView: UIScrollView!
    
    @IBOutlet weak var scheduleView: UIView!
    
    @IBOutlet weak var newsView: UIView!
    
    @IBOutlet weak var aboutView: UIView!
    
    @IBOutlet weak var photoGalleryView: UIView!
    
    @IBOutlet weak var servicesView: UIView!
    
    @IBOutlet weak var trainersView: UIView!
    
    @IBOutlet weak var contactsView: UIView!
    
    @IBOutlet weak var checkInView: UIView!
    
    @IBOutlet weak var profileView: UIView!
    
    var fieldMenuHeight:CGFloat = 60.0
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.evo_drawerController?.openDrawerGestureModeMask = .BezelPanningCenterView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screenSize: CGRect = UIScreen.main.bounds

        let screenHeight = screenSize.height
   
        fieldMenuHeight = (screenHeight - 100)/8 - 8
        
        print(fieldMenuHeight)
        
        self.scheduleView.frame = CGRect(x: 100, y: 100, width: 100, height: 100)
        
        let scheduleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainMenuVC.scheduleTapFunc))
        self.scheduleView.addGestureRecognizer(scheduleTap)
        
        let newsTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainMenuVC.newsTapFunc))
        self.newsView.addGestureRecognizer(newsTap)
        
        let aboutTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainMenuVC.aboutTapFunc))
        self.aboutView.addGestureRecognizer(aboutTap)
        
        let photoGalleryTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainMenuVC.photoGalleryTapFunc))
        self.photoGalleryView.addGestureRecognizer(photoGalleryTap)
        
        let servicesTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainMenuVC.servicesTapFunc))
        self.servicesView.addGestureRecognizer(servicesTap)
        
        let trainersTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainMenuVC.trainersTapFunc))
        self.trainersView.addGestureRecognizer(trainersTap)
        
        let contactsTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainMenuVC.contactsTapFunc))
        self.contactsView.addGestureRecognizer(contactsTap)
        
        let checkInTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainMenuVC.checkInTapFunc))
        self.checkInView.addGestureRecognizer(checkInTap)
        
        let profileTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainMenuVC.profileTapFunc))
        self.profileView.addGestureRecognizer(profileTap)
        
    }
    
    func profileTapFunc() {
        
//        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func checkInTapFunc() {
        
//        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CheckInVC") as! CheckInVC
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func contactsTapFunc() {
        
        self.contactsView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func scheduleTapFunc() {
        
        self.scheduleView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func newsTapFunc() {
        
        self.newsView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewsVC") as! NewsVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func aboutTapFunc() {
        
        self.aboutView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func photoGalleryTapFunc() {
        
        self.photoGalleryView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PhotoGalleryVC") as! PhotoGalleryVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func servicesTapFunc() {
        
        self.servicesView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ServicesVC") as! ServicesVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func trainersTapFunc() {
        
        self.trainersView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "TrainersVC") as! TrainersVC
        self.navigationController?.pushViewController(controller, animated: true)
    }

}
