//
//  Menus.swift
//  AFit
//
//  Created by Dev1 on 11/28/16.
//  Copyright © 2016 fxofficeapp. All rights reserved.
//

import Foundation

class Menus {

    fileprivate var _title: String?

    fileprivate var _icon: String?


    var title: String? {
        return _title
    }

    var icon: String? {
        return _icon
    }

    init (title: String?, icon: String?) {
        self._title = title
        self._icon = icon
    }
    
    init(dictionary: AnyObject) {
    
        if let title = dictionary["title"] as? String {
            self._title = title
        }
    
        if let icon = dictionary["icon"] as? String {
            self._icon = icon
        }
            
    }

}
